﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RPApplication.Services
{
    public interface ISettingService
    {
        void UpdateSetting(ApiModels.Setting setting);
        Models.Setting GetSetting();
    }

    public class SettingService : ISettingService
    {
        private RPContext _dbContext;
        private readonly AppSettings _appSettings;
        public SettingService(RPContext context, IOptions<AppSettings> appSettings)
        {
            _dbContext = context;
            _appSettings = appSettings.Value;
            
        }

        public void UpdateSetting(ApiModels.Setting setting)
        {
            var savedSetting = _dbContext.Settings.FirstOrDefault();

            if (savedSetting != null)
            {
                savedSetting.ShowCorridorB = setting.ShowCorridorB;
                savedSetting.ShowFacadeA = setting.ShowFacadeA;
                savedSetting.UpdatedDate = DateTime.Now;
                savedSetting.UpdatedByUserId = setting.UpdatedByUserId;
                _dbContext.Settings.Update(savedSetting);
                _dbContext.SaveChanges();
            }

        }

        public Models.Setting GetSetting()
        {
            return _dbContext.Settings.FirstOrDefault();
        }
    }
}
