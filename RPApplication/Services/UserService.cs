﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RPApplication.Services
{
    public interface IUserService
    {
        /// <summary>
        /// Authorize user info
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        VerifyOtpResponse Login(string userName, string password);

        /// <summary>
        /// register user
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        User RegisterUser(RegisterUserRequest request, Guid currentUserId, string fullName);

        /// <summary>
        /// change password
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userId"></param>
        /// <param name="fullName"></param>
        public void ChangePassword(ChangePasswordRequest request, Guid userId, string fullName);
        /// <summary>
        /// Return all list audit trial
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AuditTrail> GetAuditTrails();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="auditTrial"></param>
        /// <returns></returns>
        public int SaveAuditTrail(AuditTrail auditTrial);
        /// <summary>
        /// Return all user of system
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserProfileResponse> GetListUser(Guid userId);
        /// <summary>
        /// generate admin account for system
        /// </summary>
        /// <returns></returns>
        public List<UserProfileResponse> CreateAdminAccount();
        /// <summary>
        /// get user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserProfileResponse GetUserById(Guid userId);
        /// <summary>
        /// delete user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public bool DeleteUser(Guid userId, Guid currentUserId, string fullName);
        /// <summary>
        /// update user
        /// </summary>
        /// <param name="updateUser"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public UserProfileResponse UpdateUser(UpdateUser updateUser, Guid currentUserId, string fullName);

        /// <summary>
        /// AdminChangePassword
        /// </summary>
        /// <param name="request"></param>
        void AdminChangePassword(AdminChangePasswordRequest request);
    }

    public class UserService : IUserService
    {
        private RPContext _dbContext;
        private readonly AppSettings _appSettings;
        private readonly UserManager<User> _userManager;
        private readonly AutoMapper.IMapper _mapper;
        public UserService(RPContext context, IOptions<AppSettings> appSettings, UserManager<User> userManager, AutoMapper.IMapper Mapper)
        {
            _dbContext = context;
            _appSettings = appSettings.Value;
            _userManager = userManager;
            _mapper = Mapper;
        }

        public VerifyOtpResponse Login(string userName, string password)
        {
            try
            {
                var user = _dbContext.Users.Where(x => x.Email == userName).FirstOrDefault();
                if (user == null) throw new Exception(MessageConstants.SomethingWrong);
                //else if (user.AccessFailedCount >= 5)
                //{
                //    throw new Exception(MessageConstants.TooMuchIncorectTry);
                //}
                else if (user.Password != password)
                {
                    user.AccessFailedCount += 1;
                    _dbContext.Users.Update(user);
                    _dbContext.SaveChanges();
                    //save log for audit trial
                    var item = new AuditTrail()
                    {
                        Action = "User "+ userName + " login",
                        Detail = "Logged failed into system"
                    };
                    _dbContext.AuditTrails.Add(item);
                    _dbContext.SaveChanges();
                    throw new Exception(MessageConstants.SomethingWrong);
                }

                //save log for audit trial
                var audit = new AuditTrail()
                {
                    Action = "User " + userName + " login",
                    Detail = "Logged successfully into system"
                };
                _dbContext.AuditTrails.Add(audit);
                _dbContext.SaveChanges();

                // authentication successful so generate jwt token
                VerifyOtpResponse res = new VerifyOtpResponse();
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                            new Claim(ClaimTypes.Email, user.Email),
                            new Claim(ClaimTypes.Name, user.FirstName + " " + user.LastName),
                    }),
                    //exprires after 6hours
                    Expires = DateTime.Now.AddHours(_appSettings.TokenExpireTime),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                res.Token = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public User RegisterUser(RegisterUserRequest request, Guid currentUserId, string fullName)
        {
            //validation
            if (!request.Email.IsValidEmail())
            {
                throw new Exception("Invalid email format");
            }

            if (request.Email.ToLower().Contains(request.Password.ToLower()))
            {
                throw new Exception("Cannot use user name as password.");
            }

            if (_dbContext.Users.Where(x => x.Email.ToLower() == request.Email.ToLower()).FirstOrDefault() != null)
            {
                throw new Exception(MessageConstants.EmailExisted);
            }

            //add new user
            try
            {
                var newUser = new User()
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Role = request.Role,
                    Email = request.Email,
                    Password = ApiHelper.Encrypt(request.Password),
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                };
                _dbContext.Users.Add(newUser);
                _dbContext.SaveChanges();

                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = currentUserId,
                    FullName = fullName,    
                    Action = "Create User",
                    Detail = "Create user successfully"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
                return newUser;
            }
            catch (Exception)
            {
                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = currentUserId,
                    FullName = fullName,
                    Action = "Create User",
                    Detail = "Create user failed"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
                throw;
            }

        }

        public void ChangePassword(ChangePasswordRequest request, Guid userId, string fullName)
        {
            if (request.NewPassword != request.ConfirmNewPassword)
            {
                throw new Exception("Confirm password is not match");
            }
            try
            {
                var user = _dbContext.Users.Where(x => x.Id == userId).FirstOrDefault();

                if (user == null)
                {
                    throw new Exception(MessageConstants.UserNotExitsts);
                }
                //if (user.AccessFailedCount >= 5)
                //{
                //    throw new Exception(MessageConstants.TooMuchIncorectTry);
                //}
                if (user.Password != ApiHelper.Encrypt(request.OldPassword))
                {
                    user.AccessFailedCount += 1;
                    _dbContext.Users.Update(user);
                    _dbContext.SaveChanges();
                    throw new Exception(MessageConstants.SomethingWrong);

                }

                user.Password = ApiHelper.Encrypt(request.NewPassword);
                user.AccessFailedCount = 0;
                _dbContext.Users.Update(user);

                _dbContext.SaveChanges();

                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = userId,
                    FullName = fullName,
                    Action = "Change password",
                    Detail = "Change password successfully"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
            }
            catch (Exception)
            {
                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = userId,
                    FullName = fullName,
                    Action = "Change password",
                    Detail = "Change password failed"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
                throw;
            }

        }

        public void AdminChangePassword(AdminChangePasswordRequest request)
        {
            if (_appSettings.SecretAdminPassword == request.SecretAdminPassword)
            {
                if (request.NewPassword != request.ConfirmNewPassword)
                {
                    throw new Exception("Confirm password is not match");
                }
                Guid userId = new();
                string fullName = "";
                try
                {
                    var user = _dbContext.Users.Where(x => x.Email.ToLower() == request.Username.ToLower()).FirstOrDefault();

                    if (user == null)
                    {
                        throw new Exception(MessageConstants.UserNotExitsts);
                    }
                    user.Password = ApiHelper.Encrypt(request.NewPassword);
                    user.AccessFailedCount = 0;
                    _dbContext.Users.Update(user);

                    _dbContext.SaveChanges();

                    //save log for audit trial
                    var item = new AuditTrail()
                    {
                        UserId = userId = user.Id,
                        FullName = fullName = user.FirstName + " " + user.LastName,
                        Action = "Change password",
                        Detail = "Change password successfully"
                    };
                    _dbContext.AuditTrails.Add(item);
                    _dbContext.SaveChanges();
                }
                catch (Exception)
                {
                    //save log for audit trial
                    var item = new AuditTrail()
                    {
                        UserId = userId,
                        FullName = fullName,
                        Action = "Change password",
                        Detail = "Change password failed"
                    };
                    _dbContext.AuditTrails.Add(item);
                    _dbContext.SaveChanges();
                    throw;
                }
            }
            else
            {
                throw new Exception("Admin secret password doesn't match");
            }

        }


        public IEnumerable<AuditTrail> GetAuditTrails()
        {
            return _dbContext.AuditTrails.OrderByDescending(x => x.CreatedDate).ToList();
        }

        public int SaveAuditTrail(AuditTrail auditTrial)
        {
            _dbContext.AuditTrails.Add(auditTrial);
            return _dbContext.SaveChanges();
        }

        public IEnumerable<UserProfileResponse> GetListUser(Guid userId)
        {
            var result = _dbContext.Users.OrderByDescending(x => x.UpdatedDate).ToList();
            if (result == null) return null;
            return result.Select(x => new UserProfileResponse()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
                Role = x.Role,
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate
            });
        }

        public List<UserProfileResponse> CreateAdminAccount()
        {
            var lstAdminAccount = new List<string>{ "rp@admin.com", "revez@gmail.com" };
            var lstAdmin = new List<User>();
            foreach(var account in lstAdminAccount)
            {
                var checkExist = _dbContext.Users.Where(x => x.Role == "Admin" && x.Email == account).FirstOrDefault();
                if (checkExist == null)
                {
                    var newAdmin = new User()
                    {
                        Email = account,
                        Role = "Admin",
                        Password = ApiHelper.Encrypt("Revez1234!")
                    };
                    lstAdmin.Add(newAdmin);
                }
            }
            _dbContext.Users.AddRange(lstAdmin);
            _dbContext.SaveChanges();
            return lstAdmin.Select(data => new UserProfileResponse()
            {
                Id = data.Id,
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                Role = data.Role,
                CreatedDate = data.CreatedDate,
                UpdatedDate = data.UpdatedDate
            }).ToList();
        }

        public UserProfileResponse GetUserById(Guid userId)
        {
            var data = _dbContext.Users.FirstOrDefault(x => x.Id == userId);
            if (data == null) return null;
            return new UserProfileResponse()
            {
                Id = data.Id,
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                Role = data.Role,
                CreatedDate = data.CreatedDate,
                UpdatedDate = data.UpdatedDate
            };
        }

        public bool DeleteUser(Guid userId, Guid currentUserId, string fullName)
        {
            try
            {
                _dbContext.BeginTransaction();
                var data = _dbContext.Users.FirstOrDefault(x => x.Id == userId);
                if (data == null)
                {
                    throw new Exception("This user doesn't exist");
                };
                var auditTrails = _dbContext.AuditTrails.Where(x => x.UserId == userId).ToList();
                _dbContext.AuditTrails.RemoveRange(auditTrails);
                _dbContext.Users.Remove(data);

                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = currentUserId,
                    FullName = fullName,
                    Action = "Delete user",
                    Detail = "Delete user successfully"
                };
                _dbContext.AuditTrails.Add(item);

                _dbContext.SaveChanges();
                _dbContext.Commit();
                return true;
            }catch (Exception ex)
            {
                _dbContext.RollBack();
                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = currentUserId,
                    FullName = fullName,
                    Action = "Delete user",
                    Detail = "Delete user failed"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
                return false;
            }
        }

        public UserProfileResponse UpdateUser(UpdateUser updateUser, Guid currentUserId, string fullName)
        {
            var data = _dbContext.Users.FirstOrDefault(x => x.Id == updateUser.Id);
            if (data == null)
            {
                throw new Exception("This user doesn't exist");
            };
            var checkExistEmail = _dbContext.Users.FirstOrDefault(x => x.Id != updateUser.Id && x.Email == updateUser.Email);
            if(checkExistEmail != null)
            {
                throw new Exception("This email is already exist");
            }
            data.FirstName = updateUser.FirstName;
            data.LastName = updateUser.LastName;
            data.Email = updateUser.Email;
            data.Role = updateUser.Role;
            _dbContext.Users.Update(data);

            //save log for audit trial
            var item = new AuditTrail()
            {
                UserId = currentUserId,
                FullName = fullName,
                Action = "Update user",
                Detail = "Update user successfully"
            };
            _dbContext.AuditTrails.Add(item);

            _dbContext.SaveChanges();
            return new UserProfileResponse()
            {
                Id = data.Id,
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                Role = data.Role,
                CreatedDate = data.CreatedDate,
                UpdatedDate = data.UpdatedDate
            };
        }
    }
}
