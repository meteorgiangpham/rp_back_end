﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RPApplication.Services
{
    public interface IStationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        public StationFacadeAResponse GetStationFacade(Guid currentUserId, string domain);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        public StationCorridorBResponse GetStationCorridor(Guid currentUserId, string domain);
        /// <summary>
        /// Return list station
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public IEnumerable<Station> GetOrCreateStationList(Guid currentUserId, string fullName);
    }

    public class StationService : IStationService
    {
        private RPContext _dbContext;
        private readonly AppSettings _appSettings;
        private readonly UserManager<User> _userManager;
        private readonly AutoMapper.IMapper _mapper;
        public StationService(RPContext context, IOptions<AppSettings> appSettings, UserManager<User> userManager, AutoMapper.IMapper Mapper)
        {
            _dbContext = context;
            _appSettings = appSettings.Value;
            _userManager = userManager;
            _mapper = Mapper;
        }

        public StationFacadeAResponse GetStationFacade(Guid currentUserId, string domain)
        {
            var result = _dbContext.Stations.Include(x => x.ScreenSaver).Include(x => x.Hostpots).Include(x => x.LedScreenSave).Where(x => x.ModuleType == ModuleType.FacadeA).FirstOrDefault();
            if (result == null) return null;
            return new StationFacadeAResponse()
            {
                Id = result.Id,
                Name = result.Name,
                Description = result.Description,
                ModuleType = result.ModuleType,
                LedVideoUrl = result.LedScreenSave != null ? 
                    (!string.IsNullOrEmpty(result.LedScreenSave.LedVideoUrl) ? 
                    (domain + result.LedScreenSave.Id + "/" + result.LedScreenSave.LedVideoUrl) : result.LedScreenSave.LedVideoUrl) 
                    : null,
                BackgrounHostpotUrl = !string.IsNullOrEmpty(result.BackgroundImageHotsPot) ? (domain + result.Id + "/" + result.BackgroundImageHotsPot) : result.BackgroundImageHotsPot,
                ScreenSaver = new ScreenSaverResponse()
                {
                    Id = result.ScreenSaver.Id,
                    Body = result.ScreenSaver.Body,
                    LedVideoUrl = !string.IsNullOrEmpty(result.ScreenSaver.LedVideoUrl) ? (domain + result.ScreenSaver.Id + "/" + result.ScreenSaver.LedVideoUrl) : result.ScreenSaver.LedVideoUrl,
                    MediaUrl = !string.IsNullOrEmpty(result.ScreenSaver.MediaUrl) ? (domain + result.ScreenSaver.Id + "/" + result.ScreenSaver.MediaUrl) : result.ScreenSaver.MediaUrl,
                    Title = result.ScreenSaver.Title,
                    CreatedDate = result.ScreenSaver.CreatedDate,
                    UpdatedDate = result.ScreenSaver.UpdatedDate,
                },
                Hostpots = result.Hostpots != null && result.Hostpots.Count() > 0 ? result.Hostpots.Select(x => new HostpotResponse() { 
                    Id = x.Id,
                    Body = x.Body,
                    Title = x.Title,
                    CoordinateY = x.CoordinateY,
                    CoordinateX = x.CoordinateX,
                    HighlightMediaUrl = !string.IsNullOrEmpty(x.HighlightMediaUrl) ? (domain + x.Id + "/" + x.HighlightMediaUrl) : x.HighlightMediaUrl,
                    MediaUrl = !string.IsNullOrEmpty(x.MediaUrl) ? (domain + x.Id + "/" + x.MediaUrl) : x.MediaUrl,
                    IsVisible = x.IsVisible,
                    StationId = x.StationId,
                }) : null,
                CreatedDate = result.CreatedDate,
                UpdatedDate = result.UpdatedDate
            };
        }

        public StationCorridorBResponse GetStationCorridor(Guid currentUserId, string domain)
        {
            var result = _dbContext.Stations.Include(x => x.ScreenSaver).Include(x => x.Pages).ThenInclude(x => x.PageContentBlocks).ThenInclude(x => x.ContentBlock).Where(x => x.ModuleType == ModuleType.CorridorB).FirstOrDefault();
            if (result == null) return null;
            
            return new StationCorridorBResponse()
            {
                Id = result.Id,
                Name = result.Name,
                Description = result.Description,
                ModuleType = result.ModuleType,
                ScreenSaver = result.ScreenSaver != null ? new ScreenSaverResponse()
                {
                    Id = result.ScreenSaver.Id,
                    Body = result.ScreenSaver.Text,
                    LedVideoUrl = !string.IsNullOrEmpty(result.ScreenSaver.LedVideoUrl) ? (domain + result.ScreenSaver.Id + "/" + result.ScreenSaver.LedVideoUrl) : result.ScreenSaver.LedVideoUrl,
                    MediaUrl = !string.IsNullOrEmpty(result.ScreenSaver.MediaUrl) ? (domain + result.ScreenSaver.Id + "/" + result.ScreenSaver.MediaUrl) : result.ScreenSaver.MediaUrl,
                    Title = result.ScreenSaver.Title,
                    CreatedDate = result.ScreenSaver.CreatedDate,
                    UpdatedDate = result.ScreenSaver.UpdatedDate,
                } : null,
                Pages = result.Pages != null && result.Pages.Count() > 0 ? result.Pages.Select(x => new PageResponse()
                {
                    Id = x.Id,
                    Description = x.Description,
                    Title = x.Title,
                    SubTitle = x.SubTitle,
                    BackgroundImageUrl = !string.IsNullOrEmpty(x.BackgroundImageUrl) ? (domain + x.Id + "/" + x.BackgroundImageUrl) : x.BackgroundImageUrl,
                    IsVisible = x.IsVisible,
                    StationId = x.StationId,
                    ContentBlocks = x.PageContentBlocks.Select(x => new ContentBlockResponse()
                    {
                        Id = x.ContentBlockId,
                        Header = x.ContentBlock.Header,
                        Description = x.ContentBlock.Description,
                        ListMediaFile = !string.IsNullOrEmpty(x.ContentBlock.MediaFile) ? ApiHelper.ReturnFullPathFile(x.ContentBlock.MediaFile, (domain + x.ContentBlockId + "/")) : null,
                        IsShowMedia = x.ContentBlock.IsShowMedia,
                    })
                    
                }) : null,
                CreatedDate = result.CreatedDate,
                UpdatedDate = result.UpdatedDate
            };
        }

        public IEnumerable<Station> GetOrCreateStationList(Guid currentUserId, string fullName)
        {
            var data =  _dbContext.Stations.ToList();
            if (!data.Any())
            {

                var station1 = _dbContext.Stations.FirstOrDefault(x => x.ModuleType == ModuleType.FacadeA && x.Name == "Facade A");
                if (station1 == null)
                {
                    _dbContext.Stations.Add(new Station { Name = "Facade A", ModuleType = ModuleType.FacadeA, Description = "This is module facade A" });
                }
                var station2 = _dbContext.Stations.FirstOrDefault(x => x.ModuleType == ModuleType.CorridorB && x.Name == "Corridor B");
                if (station2 == null)
                {
                    _dbContext.Stations.Add(new Station { Name = "Corridor B", ModuleType = ModuleType.CorridorB, Description = "This is module Corridor B" });
                }
                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = currentUserId,
                    FullName = fullName,
                    Action = "Create station data",
                    Detail = "Create station data successfully"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
                data = _dbContext.Stations.ToList();
            }
            return data;
        }
    }
}
