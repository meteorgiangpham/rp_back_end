﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RPApplication.Services
{
    public interface IScreenSaverService
    {
        /// <summary>
        /// create master data for screen saver
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        IEnumerable<ScreenSaverResponse> CreateMasterDataScreenSaver(Guid userId, string fullName);
        /// <summary>
        /// create screen saver for facade A
        /// </summary>
        /// <param name="request"></param>
        /// <param name="id"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public ScreenSaver CreateScreenSaverFacadeA(CreateScreenSaverFacadeARequest request, Guid id, Guid currentUserId, string fullName);
        /// <summary>
        /// update screen saver for facade A
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        ScreenSaver UpdateScreenSaverFacadeA(UpdateScreenSaverFacadeARequest request, Guid currentUserId, string fullName);
        /// <summary>
        /// create screen saver for corridor B
        /// </summary>
        /// <param name="request"></param>
        /// <param name="id"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public ScreenSaver CreateScreenSaverCorridorB(CreateScreenSaverCorrdiorBRequest request, Guid id, Guid currentUserId, string fullName);

        /// <summary>
        /// update screen saver for corridor B
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        ScreenSaver UpdateScreenSaverCorridorB(UpdateScreenSaverCorridiorBRequest request, Guid currentUserId, string fullName);
        /// <summary>
        /// get all list screen saver
        /// </summary>
        /// <param name="stationId"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        IEnumerable<ScreenSaverResponse> GetAllScreenSaver(Guid stationId, string domain);
        /// <summary>
        ///  get screen saver by id
        /// </summary>
        /// <param name="stationId"></param>
        /// <param name="screenSaverId"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        ScreenSaverResponse GetScreenSaverById(Guid stationId, Guid screenSaverId, string domain);
        /// <summary>
        /// Create master data for led screensaver
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        LedScreenSaverResponse CreateMasterDataLedScreenSaver(Guid userId, string fullName);
        /// <summary>
        /// Update led screensaver
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        LedScreenSaver UpdateLedScreenSaver(UpdateLedScreenSaverRequest request, Guid currentUserId, string fullName);
        /// <summary>
        /// get led screensaver by id
        /// </summary>
        /// <param name="stationId"></param>
        /// <param name="ledScreenSaverId"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        LedScreenSaverResponse GetLedScreenSaverById(Guid stationId, Guid ledScreenSaverId, string domain);
        /// <summary>
        /// return list of led screensaver
        /// </summary>
        /// <param name="stationId"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        IEnumerable<LedScreenSaverResponse> GetAllLedScreenSaver(Guid stationId, string domain);

    }

    public class ScreenSaverService : IScreenSaverService
    {
        private RPContext _dbContext;
        private readonly AppSettings _appSettings;
        private readonly UserManager<User> _userManager;
        private readonly AutoMapper.IMapper _mapper;
        public ScreenSaverService(RPContext context, IOptions<AppSettings> appSettings, UserManager<User> userManager, AutoMapper.IMapper Mapper)
        {
            _dbContext = context;
            _appSettings = appSettings.Value;
            _userManager = userManager;
            _mapper = Mapper;
        }

        public ScreenSaver CreateScreenSaverFacadeA(CreateScreenSaverFacadeARequest request, Guid id, Guid currentUserId, string fullName)
        {
            var newItem = new ScreenSaver()
            {
                Title = request.Title,
                Body = request.Body,
                LedVideoUrl = request.LedVideoUrl,
                MediaUrl = request.MediaUrl,
                StationId = request.StationId,
                ThumbnailImage = request.ThumbnailUrl,
                Id = id
            };
            _dbContext.ScreenSavers.Add(newItem);
            //save log for audit trial
            var item = new AuditTrail()
            {
                UserId = currentUserId,
                FullName = fullName,
                Action = "Create screen saver facade A",
                Detail = "Create screen saver facade A successfully"
            };
            _dbContext.AuditTrails.Add(item);
            _dbContext.SaveChanges();
            return newItem;
        }

        public ScreenSaver UpdateScreenSaverFacadeA(UpdateScreenSaverFacadeARequest request, Guid currentUserId, string fullName)
        {
            var updateItem = _dbContext.ScreenSavers.Where(x => x.Id == request.Id).FirstOrDefault();
            if(updateItem != null)
            {
                updateItem.Title = request.Title;
                updateItem.Body = request.Body;
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0 && request.ImageDeletes.FirstOrDefault(x => x.Id == request.Id && x.FileName == updateItem.LedVideoUrl) != null)
                {
                    updateItem.LedVideoUrl = string.Empty;
                }
                else
                {
                    updateItem.LedVideoUrl = request.LedVideo != null && request.LedVideo.Length > 0 ? request.LedVideoUrl : updateItem.LedVideoUrl;
                }
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0 && request.ImageDeletes.FirstOrDefault(x => x.Id == request.Id && x.FileName == updateItem.MediaUrl) != null)
                {
                    updateItem.MediaUrl = string.Empty;
                }
                else
                {
                    updateItem.MediaUrl = request.Media != null && request.Media.Length > 0 ? request.MediaUrl : updateItem.MediaUrl;
                }
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0 && request.ImageDeletes.FirstOrDefault(x => x.Id == request.Id && x.FileName == updateItem.ThumbnailImage) != null)
                {
                    updateItem.ThumbnailImage = string.Empty;
                }
                else
                {
                    updateItem.ThumbnailImage = request.ThumbnailImage != null && request.ThumbnailImage.Length > 0 ? request.ThumbnailUrl : updateItem.ThumbnailImage;
                }
                _dbContext.ScreenSavers.Update(updateItem);

                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0)
                {
                    foreach (var imageDelete in request.ImageDeletes)
                    {
                        ApiHelper.DeleteFileAsync(imageDelete.Id, imageDelete.FileName);
                    }
                }
                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = currentUserId,
                    FullName = fullName,
                    Action = "Update Screen Saver Facade A",
                    Detail = "Update screen saver facade A successfully"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
            }
            
            return updateItem;
        }

        public ScreenSaver CreateScreenSaverCorridorB(CreateScreenSaverCorrdiorBRequest request, Guid id, Guid currentUserId, string fullName)
        {
            var newItem = new ScreenSaver()
            {
                Title = request.Title,
                Text = request.Text,
                MediaUrl = request.MediaUrl,
                StationId = request.StationId,
                Id = id
            };
            _dbContext.ScreenSavers.Add(newItem);
            //save log for audit trial
            var item = new AuditTrail()
            {
                UserId = currentUserId,
                FullName = fullName,
                Action = "Create screen saver corridor B",
                Detail = "Create screen saver corridor B successfully"
            };
            _dbContext.AuditTrails.Add(item);
            _dbContext.SaveChanges();
            return newItem;
        }

        public ScreenSaver UpdateScreenSaverCorridorB(UpdateScreenSaverCorridiorBRequest request, Guid currentUserId, string fullName)
        {
            var updateItem = _dbContext.ScreenSavers.Where(x => x.Id == request.Id).FirstOrDefault();
            if (updateItem != null)
            {
                updateItem.Title = request.Title;
                updateItem.Text = request.Text;
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0 && request.ImageDeletes.FirstOrDefault(x => x.Id == request.Id && x.FileName == updateItem.MediaUrl) != null)
                {
                    updateItem.MediaUrl = string.Empty;
                }
                else
                {
                    updateItem.MediaUrl = request.Media != null && request.Media.Length > 0 ? request.MediaUrl : updateItem.MediaUrl;
                }
                _dbContext.ScreenSavers.Update(updateItem);

                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0)
                {
                    foreach (var imageDelete in request.ImageDeletes)
                    {
                        ApiHelper.DeleteFileAsync(imageDelete.Id, imageDelete.FileName);
                    }
                }
                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = currentUserId,
                    FullName = fullName,
                    Action = "Update Screen Saver Corridor B",
                    Detail = "Update screen saver corridor B successfully"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
            }

            return updateItem;
        }

        public IEnumerable<ScreenSaverResponse> GetAllScreenSaver(Guid stationId, string domain)
        {
            var result = _dbContext.ScreenSavers.Include(x => x.Station).Where(x => x.StationId == stationId).ToList();
            return result.Select(data => new ScreenSaverResponse()
            {
                Id = data.Id,
                Title = data.Title,
                Body = (data.Station != null && data.Station.ModuleType == ModuleType.FacadeA) ? data.Body : data.Text,
                LedVideoUrl = !string.IsNullOrEmpty(data.LedVideoUrl) ? (domain + data.Id + "/" + data.LedVideoUrl) : data.LedVideoUrl,
                MediaUrl = !string.IsNullOrEmpty(data.MediaUrl) ? (domain + data.Id + "/" + data.MediaUrl) : data.MediaUrl,
                ThumbnailImageUrl = !string.IsNullOrEmpty(data.ThumbnailImage) ? (domain + data.Id + "/" + data.ThumbnailImage) : data.ThumbnailImage,
                CreatedDate = data.CreatedDate,
                UpdatedDate = data.UpdatedDate
            });
        }

        public ScreenSaverResponse GetScreenSaverById(Guid stationId, Guid screenSaverId, string domain)
        {
            var data = _dbContext.ScreenSavers.Include(x => x.Station).FirstOrDefault(x => x.StationId == stationId && x.Id == screenSaverId);
            if (data == null) return null;
            return new ScreenSaverResponse()
            {
                Id = data.Id,
                Title = data.Title,
                Body = (data.Station != null && data.Station.ModuleType == ModuleType.FacadeA) ? data.Body : data.Text,
                LedVideoUrl = !string.IsNullOrEmpty(data.LedVideoUrl) ? (domain + data.Id + "/" + data.LedVideoUrl) : data.LedVideoUrl,
                MediaUrl = !string.IsNullOrEmpty(data.MediaUrl) ? (domain + data.Id + "/" + data.MediaUrl) : data.MediaUrl,
                ThumbnailImageUrl = !string.IsNullOrEmpty(data.ThumbnailImage) ? (domain + data.Id + "/" + data.ThumbnailImage) : data.ThumbnailImage,
                CreatedDate = data.CreatedDate,
                UpdatedDate = data.UpdatedDate
            };
        }

        public IEnumerable<ScreenSaverResponse> CreateMasterDataScreenSaver(Guid userId, string fullName)
        {
            var station1 = _dbContext.Stations.FirstOrDefault(x => x.ModuleType == ModuleType.FacadeA && x.Name == "Facade A");
            var station2 = _dbContext.Stations.FirstOrDefault(x => x.ModuleType == ModuleType.CorridorB && x.Name == "Corridor B");
            if (station1 != null && station2 != null)
            {
                var screenSaverA = _dbContext.ScreenSavers.FirstOrDefault(x => x.StationId == station1.Id);
                if(screenSaverA == null)
                {
                    var screenSaverFacade = new ScreenSaver()
                    {
                        StationId = station1.Id,
                        Title = "Screen saver facade A",
                    };
                    _dbContext.ScreenSavers.Add(screenSaverFacade);
                }

                var screenSaverB = _dbContext.ScreenSavers.FirstOrDefault(x => x.StationId == station2.Id);
                if (screenSaverB == null)
                {
                    var screenSaverCorridor = new ScreenSaver()
                    {
                        StationId = station2.Id,
                        Title = "Screen saver corridor B",
                    };
                    _dbContext.ScreenSavers.Add(screenSaverCorridor);
                }
                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = userId,
                    FullName = fullName,
                    Action = "Create Screen Saver",
                    Detail = "Create master data for screen saver"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
            }
            var lstScreen = _dbContext.ScreenSavers.ToList();
            if(lstScreen != null && lstScreen.Count > 0)
            {
                return lstScreen.Select(x => new ScreenSaverResponse()
                {
                    Id = x.Id,
                    Title = x.Title,
                    Body = x.Body,
                    LedVideoUrl = x.LedVideoUrl,
                    MediaUrl = x.MediaUrl,
                    ThumbnailImageUrl = x.ThumbnailImage,
                    CreatedDate = x.CreatedDate,
                    UpdatedDate = x.UpdatedDate
                });
            } else
            {
                return null;
            }
        }

        public LedScreenSaverResponse CreateMasterDataLedScreenSaver(Guid userId, string fullName)
        {
            var station1 = _dbContext.Stations.FirstOrDefault(x => x.ModuleType == ModuleType.FacadeA && x.Name == "Facade A");
            if (station1 != null)
            {
                var screenSaver = _dbContext.LedScreenSavers.FirstOrDefault(x => x.StationId == station1.Id);
                if (screenSaver == null)
                {
                    var ledScreenSaver = new LedScreenSaver()
                    {
                        StationId = station1.Id
                    };
                    _dbContext.LedScreenSavers.Add(ledScreenSaver);
                }

                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = userId,
                    FullName = fullName,
                    Action = "Create Led Screen Saver",
                    Detail = "Create master data for led screen saver"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
            }
            var ledScreen = _dbContext.LedScreenSavers.FirstOrDefault();
            if (ledScreen != null)
            {
                return new LedScreenSaverResponse()
                {
                    Id = ledScreen.Id,
                    LedVideoUrl = ledScreen.LedVideoUrl,
                    ThumbnailImageUrl = ledScreen.ThumbnailImage,
                    CreatedDate = ledScreen.CreatedDate,
                    UpdatedDate = ledScreen.UpdatedDate
                };
            }else
            {
                return null;
            }
            
        }

        public LedScreenSaver UpdateLedScreenSaver(UpdateLedScreenSaverRequest request, Guid currentUserId, string fullName)
        {
            var updateItem = _dbContext.LedScreenSavers.Where(x => x.Id == request.Id).FirstOrDefault();
            if (updateItem != null)
            {
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0 && request.ImageDeletes.FirstOrDefault(x => x.Id == request.Id && x.FileName == updateItem.LedVideoUrl) != null)
                {
                    updateItem.LedVideoUrl = string.Empty;
                }
                else
                {
                    updateItem.LedVideoUrl = request.LedVideo != null && request.LedVideo.Length > 0 ? request.LedVideoUrl : updateItem.LedVideoUrl;
                }
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0 && request.ImageDeletes.FirstOrDefault(x => x.Id == request.Id && x.FileName == updateItem.ThumbnailImage) != null)
                {
                    updateItem.ThumbnailImage = string.Empty;
                }
                else
                {
                    updateItem.ThumbnailImage = request.Thumbnail != null && request.Thumbnail.Length > 0 ? request.ThumbnailUrl : updateItem.ThumbnailImage;
                }
                _dbContext.LedScreenSavers.Update(updateItem);

                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0)
                {
                    foreach (var imageDelete in request.ImageDeletes)
                    {
                        ApiHelper.DeleteFileAsync(imageDelete.Id, imageDelete.FileName);
                    }
                }
                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = currentUserId,
                    FullName = fullName,
                    Action = "Update Led Screen Saver",
                    Detail = "Update led screen saver successfully"
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
            }

            return updateItem;
        }

        public LedScreenSaverResponse GetLedScreenSaverById(Guid stationId, Guid ledScreenSaverId, string domain)
        {
            var data = _dbContext.LedScreenSavers.FirstOrDefault(x => x.StationId == stationId && x.Id == ledScreenSaverId);
            if (data == null) return null;
            return new LedScreenSaverResponse()
            {
                Id = data.Id,
                LedVideoUrl = !string.IsNullOrEmpty(data.LedVideoUrl) ? (domain + data.Id + "/" + data.LedVideoUrl) : data.LedVideoUrl,
                ThumbnailImageUrl = !string.IsNullOrEmpty(data.ThumbnailImage) ? (domain + data.Id + "/" + data.ThumbnailImage) : data.ThumbnailImage,
                CreatedDate = data.CreatedDate,
                UpdatedDate = data.UpdatedDate
            };
        }

        public IEnumerable<LedScreenSaverResponse> GetAllLedScreenSaver(Guid stationId, string domain)
        {
            var result = _dbContext.LedScreenSavers.Where(x => x.StationId == stationId).ToList();
            return result.Select(data => new LedScreenSaverResponse()
            {
                Id = data.Id,
                LedVideoUrl = !string.IsNullOrEmpty(data.LedVideoUrl) ? (domain + data.Id + "/" + data.LedVideoUrl) : data.LedVideoUrl,
                ThumbnailImageUrl = !string.IsNullOrEmpty(data.ThumbnailImage) ? (domain + data.Id + "/" + data.ThumbnailImage) : data.ThumbnailImage,
                CreatedDate = data.CreatedDate,
                UpdatedDate = data.UpdatedDate
            });
        }
    }
}
