﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RPApplication.Services
{
    public interface IPageService
    {
        /// <summary>
        /// create master data for page
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public IEnumerable<PageResponse> CreatePage(Guid currentUserId, string fullName);
        /// <summary>
        /// Update page
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public Page UpdatePage(UpdatePageRequest request, Guid currentUserId, string fullName);
        /// <summary>
        /// Get page by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        public PageResponse GetPageById(Guid id, string domain);
        /// <summary>
        /// get all page
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public IEnumerable<PageResponse> GetPageAll(string domain);
    }

    public class PageService : IPageService
    {
        private RPContext _dbContext;
        private readonly AppSettings _appSettings;
        private readonly UserManager<User> _userManager;
        private readonly AutoMapper.IMapper _mapper;
        public PageService(RPContext context, IOptions<AppSettings> appSettings, UserManager<User> userManager, AutoMapper.IMapper Mapper)
        {
            _dbContext = context;
            _appSettings = appSettings.Value;
            _userManager = userManager;
            _mapper = Mapper;
        }

        public IEnumerable<PageResponse> CreatePage(Guid currentUserId, string fullName)
        {
            try
            {
                _dbContext.BeginTransaction();
                var lstPage = new List<Page>();
                var stationCorridor = _dbContext.Stations.Where(x => x.ModuleType == ModuleType.CorridorB).FirstOrDefault();
                if (stationCorridor != null)
                {
                    var checkExistPage = _dbContext.Pages.ToList();
                    if (checkExistPage == null || (checkExistPage != null && checkExistPage.Count() == 0))
                    {
                        for (int i = 1; i <= 8; ++i)
                        {
                            var newItem = new Page()
                            {
                                Title = "Page " + i,
                                IsVisible = true,
                                StationId = stationCorridor.Id,
                            };
                            lstPage.Add(newItem);
                        }
                        _dbContext.Pages.AddRange(lstPage);

                        //save log for audit trial
                        var item = new AuditTrail()
                        {
                            UserId = currentUserId,
                            FullName = fullName,
                            Action = "Create page",
                            Detail = "Create page successfully",
                        };
                        _dbContext.AuditTrails.Add(item);
                        _dbContext.SaveChanges();
                        _dbContext.Commit();
                        return lstPage.Select(x => new PageResponse()
                        {
                            Id = x.Id,
                            Title = x.Title,
                            CreatedDate = x.CreatedDate,
                            UpdatedDate = x.UpdatedDate
                        });
                    } else
                    {
                        return checkExistPage.Select(x => new PageResponse()
                        {
                            Id = x.Id,
                            Title = x.Title,
                            SubTitle = x.SubTitle,
                            BackgroundImageUrl = x.BackgroundImageUrl,
                            StationId = x.StationId,
                            Description = x.Description,
                            IsVisible = x.IsVisible,
                            CreatedDate = x.CreatedDate,
                            UpdatedDate = x.UpdatedDate,
                        });
                    }
                } else
                {
                    throw new Exception("No station found");
                }
            } catch (Exception ex)
            {
                _dbContext.RollBack();
                throw;
            }
        }

        public Page UpdatePage(UpdatePageRequest request, Guid currentUserId, string fullName)
        {
            try
            {
                _dbContext.BeginTransaction();
                var updateItem = _dbContext.Pages.Where(x => x.Id == request.Id).FirstOrDefault();
                if (updateItem != null)
                {
                    var lstDeleteImageIds = new List<Guid>();
                    if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0)
                    {
                        lstDeleteImageIds = request.ImageDeletes.Select(x => x.Id).ToList();
                        var lstContentBlockUpdateImage = new List<ContentBlock>();
                        foreach (var imageDelete in request.ImageDeletes)
                        {
                            ApiHelper.DeleteFileAsync(imageDelete.Id, imageDelete.FileName);
                            var contentBlockMedia = _dbContext.ContentBlocks.FirstOrDefault(x => x.Id == imageDelete.Id && x.MediaFile.Contains(imageDelete.FileName));
                            var contentBlockThumnail = _dbContext.ContentBlocks.FirstOrDefault(x => x.Id == imageDelete.Id && x.Thumbnail.Contains(imageDelete.FileName));
                            if (contentBlockMedia != null)
                            {
                                var lstContent = contentBlockMedia.MediaFile.Split(';');
                                var lst = lstContent.Where(x => !x.Equals(imageDelete.FileName));
                                contentBlockMedia.MediaFile = lstContent.Count() > 0 ? string.Join(';', lst) : string.Empty;
                                lstContentBlockUpdateImage.Add(contentBlockMedia);
                            }
                            if (contentBlockThumnail != null)
                            {
                                var lstContent = contentBlockMedia.Thumbnail.Split(';');
                                var lst = lstContent.Where(x => !x.Equals(imageDelete.FileName));
                                contentBlockMedia.Thumbnail = lstContent.Count() > 0 ? string.Join(';', lst) : string.Empty;
                                lstContentBlockUpdateImage.Add(contentBlockThumnail);
                            }
                            if (lstContentBlockUpdateImage != null && lstContentBlockUpdateImage.Count() > 0)
                            {
                                _dbContext.ContentBlocks.UpdateRange(lstContentBlockUpdateImage);
                            }
                        }
                    }
                    updateItem.Title = request.Title;
                    if (lstDeleteImageIds.Count > 0 && lstDeleteImageIds.Contains(request.Id))
                    {
                        updateItem.BackgroundImageUrl = String.Empty;
                    }
                    else
                    {
                        updateItem.BackgroundImageUrl = request.Background != null && request.Background.Length > 0 ? request.BackgroundUrl : updateItem.BackgroundImageUrl;
                    }
                    updateItem.IsVisible = request.IsVisible;
                    updateItem.Description = request.Description;
                    updateItem.SubTitle = request.SubTitle;
                    _dbContext.Pages.Update(updateItem);

                    //Delete contentblock
                    if (request.ContentBlockDeletes != null && request.ContentBlockDeletes.Count() > 0)
                    {
                        var lstContentBlockId = new List<Guid>();
                        var lstPageContentBlockDel = new List<PageContentBlock>();
                        foreach (var itemDelete in request.ContentBlockDeletes)
                        {
                            var pageContentBlockDel = _dbContext.PageContentBlocks.Where(x => x.PageId == itemDelete.PageId && x.ContentBlockId == itemDelete.ContentBlockId).FirstOrDefault(); 
                            if (pageContentBlockDel != null)
                            {
                                lstPageContentBlockDel.Add(pageContentBlockDel);
                                lstContentBlockId.Add(itemDelete.ContentBlockId);
                            }
                        }
                        if (lstPageContentBlockDel != null && lstPageContentBlockDel.Count() > 0)
                        {
                            _dbContext.PageContentBlocks.RemoveRange(lstPageContentBlockDel);
                            var lstContentBlockDel = _dbContext.ContentBlocks.Where(x => lstContentBlockId.Contains(x.Id));
                            _dbContext.ContentBlocks.RemoveRange(lstContentBlockDel);
                        }
                    }

                    if (request.ContentBlocks != null && request.ContentBlocks.Count() > 0)
                    {
                        var lstBlock = new List<ContentBlock>();
                        var lstBlockUpdate = new List<ContentBlock>();
                        foreach (var x in request.ContentBlocks)
                        {
                            var checkExistBlock = _dbContext.ContentBlocks.Where(a => a.Id == x.Id).FirstOrDefault();
                            if (checkExistBlock == null)
                            {
                                var newBlock = new ContentBlock()
                                {
                                    Id = x.Id,
                                    Header = x.Header,
                                    Description = x.Description,
                                    IsShowMedia = x.IsShowMedia,
                                    MediaFile = x.MediaFile,
                                };
                                lstBlock.Add(newBlock);
                            }
                            else
                            {
                                checkExistBlock.MediaFile = x.MediaThumbnails != null && x.MediaThumbnails.Count() > 0 ? x.MediaFile : checkExistBlock.MediaFile;
                                checkExistBlock.IsShowMedia = x.IsShowMedia;
                                checkExistBlock.Header = x.Header;
                                checkExistBlock.Description = x.Description;
                                lstBlockUpdate.Add(checkExistBlock);
                            }
                        }

                        _dbContext.ContentBlocks.AddRange(lstBlock);
                        _dbContext.ContentBlocks.UpdateRange(lstBlockUpdate);
                        _dbContext.SaveChanges();
                        if (lstBlock != null && lstBlock.Count() > 0)
                        {
                            var lstPageContentBlock = lstBlock.Select(item => new PageContentBlock()
                            {
                                ContentBlockId = item.Id,
                                PageId = updateItem.Id
                            });
                            _dbContext.PageContentBlocks.AddRange(lstPageContentBlock);
                        }
                    }
                    //save log for audit trial
                    var item = new AuditTrail()
                    {
                        UserId = currentUserId,
                        FullName = fullName,
                        Action = "Update Page",
                        Detail = "Update page successfully",
                    };
                    _dbContext.AuditTrails.Add(item);
                    _dbContext.SaveChanges();
                    _dbContext.Commit();
                }

                return updateItem;
            }
            catch (Exception ex)
            {
                _dbContext.RollBack();
                throw;
            }
        }

        public PageResponse GetPageById(Guid id, string domain)
        {
            var data = _dbContext.Pages.Include(x => x.PageContentBlocks).ThenInclude(x => x.ContentBlock).FirstOrDefault(x => x.Id == id);
            if(data == null) return null;
            return new PageResponse()
            {
                Id = id,
                BackgroundImageUrl = !string.IsNullOrEmpty(data.BackgroundImageUrl) ? (domain + id + "/" + data.BackgroundImageUrl) : data.BackgroundImageUrl,
                Title = data.Title,
                SubTitle = data.SubTitle,
                Description = data.Description,
                StationId = data.StationId,
                IsVisible = data.IsVisible,
                ContentBlocks = data.PageContentBlocks != null && data.PageContentBlocks.Count() > 0 ? data.PageContentBlocks.Select(x => new ContentBlockResponse()
                {
                    Id = x.ContentBlockId,
                    Description = x.ContentBlock.Description,
                    Header = x.ContentBlock.Header,
                    ListMediaFile = !string.IsNullOrEmpty(x.ContentBlock.MediaFile) ? ApiHelper.ReturnFullPathFile(x.ContentBlock.MediaFile, (domain + x.ContentBlockId + "/")) : null,
                    IsShowMedia = x.ContentBlock.IsShowMedia,
                }) : new List<ContentBlockResponse>(),
                CreatedDate = data.CreatedDate,
                UpdatedDate = data.UpdatedDate,
            };
        }


        public IEnumerable<PageResponse> GetPageAll(string domain)
        {
            var data = _dbContext.Pages.Include(x => x.PageContentBlocks).ThenInclude(x => x.ContentBlock).ToList();
            return data.Select(x => new PageResponse()
            {
                Id = x.Id,
                Title = x.Title,
                SubTitle = x.SubTitle,
                BackgroundImageUrl = !string.IsNullOrEmpty(x.BackgroundImageUrl) ? (domain + x.Id + "/" + x.BackgroundImageUrl) : x.BackgroundImageUrl,
                StationId = x.StationId,
                ContentBlocks = x.PageContentBlocks != null ? x.PageContentBlocks.Select(x => new ContentBlockResponse()
                {
                    Id = x.ContentBlock.Id,
                    Header = x.ContentBlock.Header,
                    Description = x.ContentBlock.Description,
                    IsShowMedia = x.ContentBlock.IsShowMedia,
                    ListMediaFile = !string.IsNullOrEmpty(x.ContentBlock.MediaFile) ? ApiHelper.ReturnFullPathFile(x.ContentBlock.MediaFile, (domain + x.ContentBlockId + "/")) : null,
                }) : new List<ContentBlockResponse>(),
                Description = x.Description,
                IsVisible = x.IsVisible,
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate,
            });
        }
    }
}
