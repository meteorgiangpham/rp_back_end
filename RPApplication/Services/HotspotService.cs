﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RPApplication.Services
{
    public interface IHotspotService
    {
        /// <summary>
        /// create list hostpot master data
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public IEnumerable<HostpotResponse> CreateHostpot(Guid currentUserId, string fullName);
        /// <summary>
        /// update hostpot
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public Hostpot UpdateHostpot(UpdateHostpotRequest request, Guid currentUserId, string fullName);
        /// <summary>
        /// get hostpot by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HostpotResponse GetHostpotById(Guid id, string domain);
        /// <summary>
        /// get all hostpot
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public IEnumerable<HostpotResponse> GetAllHostpot(string domain);
        /// <summary>
        /// create hostpot background image
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public Station CreateHostpotBackgroundImage(CreateHostpotBackgroundRequest request, Guid currentUserId, string fullName);
        /// <summary>
        /// return background of hostpot page
        /// </summary>
        /// <param name="stationId"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        public HostpotBackgroundResponse GetBackgroundHostpot(Guid stationId, string domain);
    }

    public class HotspotService : IHotspotService
    {
        private RPContext _dbContext;
        private readonly AppSettings _appSettings;
        private readonly UserManager<User> _userManager;
        private readonly AutoMapper.IMapper _mapper;
        public HotspotService(RPContext context, IOptions<AppSettings> appSettings, UserManager<User> userManager, AutoMapper.IMapper Mapper)
        {
            _dbContext = context;
            _appSettings = appSettings.Value;
            _userManager = userManager;
            _mapper = Mapper;
        }

        public IEnumerable<HostpotResponse> CreateHostpot(Guid currentUserId, string fullName)
        {
            try
            {
                _dbContext.BeginTransaction();
                var lstHostpot = new List<Hostpot>();
                var stationFacde = _dbContext.Stations.Where(x => x.ModuleType == ModuleType.FacadeA).FirstOrDefault();
                if (stationFacde != null)
                {
                    var checkExistHostpot = _dbContext.Hostpots.ToList();
                    if (checkExistHostpot == null || (checkExistHostpot != null && checkExistHostpot.Count() == 0))
                    {
                        for (int i = 1; i <= 5; ++i)
                        {
                            var newItem = new Hostpot()
                            {
                                Title = "Hostpot " + i,
                                IsVisible = true,
                                StationId = stationFacde.Id,
                            };
                            lstHostpot.Add(newItem);
                        }
                        _dbContext.Hostpots.AddRange(lstHostpot);
                        //save log for audit trial
                        var item = new AuditTrail()
                        {
                            UserId = currentUserId,
                            FullName = fullName,
                            Action = "Create hostpot",
                            Detail = "Create hostpot successfully",
                        };
                        _dbContext.AuditTrails.Add(item);
                        _dbContext.SaveChanges();
                        _dbContext.Commit();
                        return lstHostpot.Select(x => new HostpotResponse()
                        {
                            Id = x.Id,
                            Title = x.Title,
                            IsVisible = x.IsVisible,
                            CreatedDate = x.CreatedDate,
                            UpdatedDate = x.UpdatedDate
                        });
                    }
                    else
                    {
                        return checkExistHostpot.Select(x => new HostpotResponse()
                        {
                            Id = x.Id,
                            Body = x.Body,
                            CoordinateY = x.CoordinateY,
                            CoordinateX = x.CoordinateX,
                            HighlightMediaUrl = x.HighlightMediaUrl,
                            MediaUrl = x.MediaUrl,
                            Title = x.Title,
                            IsVisible = x.IsVisible,
                            StationId = x.StationId,
                            CreatedDate = x.CreatedDate,
                            UpdatedDate = x.UpdatedDate
                        });
                    }
                }
                else
                {
                    throw new Exception("No station found");
                }
            }catch( Exception ex)
            {
                _dbContext.RollBack();
                throw;
            }
        }

        public Hostpot UpdateHostpot(UpdateHostpotRequest request, Guid currentUserId, string fullName)
        {
            var updateItem = _dbContext.Hostpots.Where(x => x.Id == request.Id).FirstOrDefault();
            if(updateItem != null)
            {
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0)
                {
                    foreach (var imageDelete in request.ImageDeletes)
                    {
                        ApiHelper.DeleteFileAsync(imageDelete.Id, imageDelete.FileName);
                    }
                }

                updateItem.Title = request.Title;
                updateItem.Body = request.Body;
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0 && request.ImageDeletes.FirstOrDefault(x => x.Id == request.Id && x.FileName == updateItem.MediaUrl) != null)
                {
                    updateItem.MediaUrl = string.Empty;
                } else
                {
                    updateItem.MediaUrl = request.Media != null && request.Media.Length > 0 ? request.MediaUrl : updateItem.MediaUrl;
                }
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0 && request.ImageDeletes.FirstOrDefault(x => x.Id == request.Id && x.FileName == updateItem.ThumnailImage) != null)
                {
                    updateItem.ThumnailImage = string.Empty;
                }
                else
                {
                    updateItem.ThumnailImage = request.Thumbnail != null && request.Thumbnail.Length > 0 ? request.ThumbnailUrl : updateItem.ThumnailImage;
                }
                updateItem.IsVisible = request.IsVisible;
                updateItem.CoordinateX = request.CoordinateX;
                updateItem.CoordinateY = request.CoordinateY;
                if (request.ImageDeletes != null && request.ImageDeletes.Count() > 0 && request.ImageDeletes.FirstOrDefault(x => x.Id == request.Id && x.FileName == updateItem.HighlightMediaUrl) != null)
                {
                    updateItem.HighlightMediaUrl = string.Empty;
                }
                else
                {
                    updateItem.HighlightMediaUrl = request.HighLightMedia != null && request.HighLightMedia.Length > 0 ? request.HighlightMediaUrl : updateItem.HighlightMediaUrl;
                }
                _dbContext.Hostpots.Update(updateItem);

                //save log for audit trial
                var item = new AuditTrail()
                {
                    UserId = currentUserId,
                    FullName = fullName,
                    Action = "Update Hostpot",
                    Detail = "Update hostpot successfully",
                };
                _dbContext.AuditTrails.Add(item);
                _dbContext.SaveChanges();
            }
            
            return updateItem;
        }

        public HostpotResponse GetHostpotById(Guid id, string domain)
        {
            var data = _dbContext.Hostpots.FirstOrDefault(x => x.Id == id);
            if (data == null) return null;
            return new HostpotResponse()
            {
                Id = data.Id,
                Body = data.Body,
                CoordinateY = data.CoordinateY,
                CoordinateX = data.CoordinateX,
                HighlightMediaUrl = !string.IsNullOrEmpty(data.HighlightMediaUrl) ? (domain + data.Id + "/" + data.HighlightMediaUrl) : data.HighlightMediaUrl,
                MediaUrl = !string.IsNullOrEmpty(data.MediaUrl) ? (domain + data.Id + "/" + data.MediaUrl) : data.MediaUrl,
                ThumbnailUrl = !string.IsNullOrEmpty(data.ThumnailImage) ? (domain + data.Id + "/" + data.ThumnailImage) : data.ThumnailImage,
                HostpotImageUrl = domain + "hostpot.png",
                Title = data.Title,
                IsVisible = data.IsVisible,
                StationId = data.StationId,
                CreatedDate = data.CreatedDate,
                UpdatedDate = data.UpdatedDate,
            };
        }

        public IEnumerable<HostpotResponse> GetAllHostpot(string domain)
        {
            var data = _dbContext.Hostpots.Include(x => x.Station).ToList();
            return data.Select(x => new HostpotResponse() { 
                Id = x.Id,
                Body = x.Body,
                BackgroundHostpotImage = x.Station != null ? !string.IsNullOrEmpty(x.Station.BackgroundImageHotsPot) ? 
                (domain + x.Station.Id + "/" + x.Station.BackgroundImageHotsPot) : x.Station.BackgroundImageHotsPot : null,
                CoordinateY = x.CoordinateY,
                CoordinateX = x.CoordinateX,
                HighlightMediaUrl = !string.IsNullOrEmpty(x.HighlightMediaUrl) ? (domain + x.Id + "/" + x.HighlightMediaUrl) : x.HighlightMediaUrl,
                MediaUrl = !string.IsNullOrEmpty(x.MediaUrl) ? (domain + x.Id + "/" + x.MediaUrl) : x.MediaUrl,
                ThumbnailUrl = !string.IsNullOrEmpty(x.ThumnailImage) ? (domain + x.Id + "/" + x.ThumnailImage) : x.ThumnailImage,
                HostpotImageUrl = domain + "hostpot.png",
                Title = x.Title,
                IsVisible = x.IsVisible,
                StationId = x.StationId,
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate,
            });
        }

        public Station CreateHostpotBackgroundImage(CreateHostpotBackgroundRequest request, Guid currentUserId, string fullName)
        {
            var station = _dbContext.Stations.FirstOrDefault(x => x.Id == request.StationId);
            if(station == null)
            {
                return null;
            }

            if(request.BackgroundHostpot != null && request.BackgroundHostpot.Length > 0)
            {
                station.BackgroundImageHotsPot = request.BackgroundHostpotUrl;
            }
            _dbContext.Stations.Update(station);
            //save log for audit trial
            var item = new AuditTrail()
            {
                UserId = currentUserId,
                FullName = fullName,
                Action = "Create background hostpot",
                Detail = "Create background hostpot successfully",
            };
            _dbContext.AuditTrails.Add(item);
            _dbContext.SaveChanges();
            return station;
        }

        public HostpotBackgroundResponse GetBackgroundHostpot(Guid stationId, string domain)
        {
            var data = _dbContext.Stations.FirstOrDefault(x => x.Id == stationId);
            if(data == null)
            {
                return null;
            }
            return new HostpotBackgroundResponse()
            {
                StationId = stationId,
                BackgroundHostpotUrl = !string.IsNullOrEmpty(data.BackgroundImageHotsPot) ? (domain + data.Id + "/" + data.BackgroundImageHotsPot) : data.BackgroundImageHotsPot,
            };
           
        }
    }
}
