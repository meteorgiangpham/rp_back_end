﻿using System;

namespace RPApplication.Models
{
    public class Hostpot
    {
        public Guid Id { get; set; }
        public Guid StationId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string HighlightMediaUrl { get; set; }
        public string ThumnailImage { get; set; }
        public string MediaUrl { get; set; }
        public string CoordinateX { get; set; }
        public string CoordinateY { get; set; }
        public bool IsVisible { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual Station Station { get; set; }

        public Hostpot()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
    }
}
