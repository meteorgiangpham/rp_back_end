﻿using System;

namespace RPApplication.Models
{
    public class PageContentBlock
    {
        public Guid Id { get; set; }
        public Guid PageId { get; set; }
        public Guid ContentBlockId { get; set; }
        public virtual Page Page { get; set; }
        public virtual ContentBlock ContentBlock { get; set; }
    }
}
