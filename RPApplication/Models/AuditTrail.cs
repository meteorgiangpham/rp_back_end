﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RPApplication.Models
{
    public class AuditTrail
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public string FullName { get; set; }
        public string Action { get; set; }
        public string Detail { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual User User { get; set; }
        public AuditTrail()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }

    }
}
