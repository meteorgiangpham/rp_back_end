﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RPApplication.Models
{

    public class Setting
    {
        public Guid Id { get; private set; }

        public bool ShowFacadeA { get; set; }

        public bool ShowCorridorB { get; set; }

        public DateTime UpdatedDate { get; set; }

        public Guid? UpdatedByUserId { get; set; }

    }
}
