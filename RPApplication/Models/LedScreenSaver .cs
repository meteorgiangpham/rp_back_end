﻿using System;

namespace RPApplication.Models
{
    public class LedScreenSaver
    {
        public Guid Id { get; set; }
        public Guid StationId { get; set; }
        public string ThumbnailImage { get; set; }
        public string LedVideoUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual Station Station { get; set; }

        public LedScreenSaver()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }

    }
}
