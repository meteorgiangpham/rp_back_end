﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace RPApplication.Models
{
    public interface IDataContext
    {
        DbSet<User> Users { get; set; }
        DbSet<ScreenSaver> ScreenSavers{ get; set; }
        DbSet<Station> Stations { get; set; }
        DbSet<ContentBlock> ContentBlocks { get; set; }
        DbSet<AuditTrail> AuditTrails { get; set; }
        DbSet<PageContentBlock> PageContentBlocks { get; set; }
        DbSet<Page> Pages { get; set; }
        DbSet<Hostpot> Hostpots { get; set; }
        DbSet<Setting> Settings { get; set; }

        void BeginTransaction();
        void Commit();
        void RollBack();
    }
}
