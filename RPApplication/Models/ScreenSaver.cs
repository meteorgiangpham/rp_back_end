﻿using System;

namespace RPApplication.Models
{
    public class ScreenSaver
    {
        public Guid Id { get; set; }
        public Guid StationId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Text { get; set; }
        public string ThumbnailImage { get; set; }
        public string LedVideoUrl { get; set; }
        public string MediaUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual Station Station { get; set; }

        public ScreenSaver()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }

    }
}
