﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace RPApplication.Models
{
    public class User : IdentityUser
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string Avatar { get; set; }
        public string TemporaryPassword { get;set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual ICollection<AuditTrail> AuditTrials { get; set; }
        public User()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now; 
        }

    }
}
