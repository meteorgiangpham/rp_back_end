﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace RPApplication.Models
{
    public class RPContext : DbContext, IDataContext
    {
        public RPContext(DbContextOptions<RPContext> options)
            : base(options)
        {
        }
        private IDbContextTransaction _transaction;

        public DbSet<User> Users { get; set; }
        public DbSet<ScreenSaver> ScreenSavers{ get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<ContentBlock> ContentBlocks { get; set; }
        public DbSet<AuditTrail> AuditTrails { get; set; }
        public DbSet<PageContentBlock> PageContentBlocks { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Hostpot> Hostpots { get; set; }
        public DbSet<LedScreenSaver> LedScreenSavers { get; set; }

        public DbSet<Setting> Settings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
         
        }

        public void BeginTransaction()
        {
            _transaction = Database.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                SaveChanges();
                _transaction.Commit();
            }
            finally
            {
                _transaction.Dispose();
            }
        }

        public void RollBack()
        {
            _transaction?.Rollback();
            _transaction?.Dispose();
        }

    }
}
