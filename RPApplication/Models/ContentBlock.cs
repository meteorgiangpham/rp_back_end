﻿using System;
using System.Collections.Generic;

namespace RPApplication.Models
{
    public class ContentBlock
    {
        public Guid Id { get; set; }
        public string Header { get; set;}
        public string Description { get; set; }
        public bool IsShowMedia { get; set; }
        public string MediaFile { get; set; }
        public string Thumbnail { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual ICollection<PageContentBlock> StationContentBlocks { get; set; }
        public ContentBlock()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }

    }
}
