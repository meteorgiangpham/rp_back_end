﻿using RPApplication.Helpers;
using System;
using System.Collections.Generic;

namespace RPApplication.Models
{
    public class Station
    {
        public Guid Id { get; set; }
        public ModuleType ModuleType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string BackgroundImageHotsPot { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual ScreenSaver ScreenSaver { get; set; }
        public virtual LedScreenSaver LedScreenSave { get; set; }
        public virtual ICollection<Hostpot> Hostpots { get; set; }
        public virtual ICollection<Page> Pages { get; set; }

        public Station()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
    }
}
