﻿using System;
using System.Collections.Generic;

namespace RPApplication.Models
{
    public class Page
    {
        public Guid Id { get; set; }
        public Guid StationId { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public string BackgroundImageUrl { get; set; }
        public bool IsVisible { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual Station Station { get; set; }
        public virtual ICollection<PageContentBlock> PageContentBlocks { get; set; }

        public Page()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
    }
}
