﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RPApplication.Helpers
{
    public class Constants
    {
        public const string AppSettings = "AppSettings";
        public const string Secret = "Secret";
        public const string EmailTemplateFolder = "EmailTemplate";
        public const string TemplateRegisterEmail = "register.html";
        public const string TemplateWelcomeEmail = "welcome.html";
        public const string TemplateForgotPasswordEmail = "forgot_pass.html";
        public const string TemplateConfirmChangePassword = "confirm_forgot_password.html";
        public const string TemplateResendOTP = "resend_otp.html";
        public const string TemplateLoginOTP = "login.html";
        public const string TemplateConfirmRedeem = "confirm_redeem_voucher.html";
        public const string SubjectAccountVerifyEmail = "Kampong Gelam Virtual Experience – Account Verification";
        public const string SubjectWelcomeEmail = "Kampong Gelam Virtual Experience – Welcome";
        public const string SubjectResetPasswordRequestEmail = "[Request for Password Reset] Kampong Gelam Virtual Experience";
        public const string SubjectResetPasswordConfirmEmail = "[Confirmation of Password Reset] Kampong Gelam Virtual Experience";
        public const string SubjectChangePasswordConfirmEmail = "[Confirmation of Password Change] Kampong Gelam Virtual Experience";
        public const string SubjectConfirmRedeemVoucher = "Voucher Redemption Email";
        public const string UploadFolder = "Upload";
        public const string ScreenSaverFolder = "ScreenSaver";

        public const string TreasureHunt = "TreasureHunt";
        public const string ARFind = "ARFind";
    }

    public class MessageConstants
    {
        public const string EmailExisted = "This email is already existed!";
        public const string EmailNotExists = "This email does not exists!";
        public const string UserNotExitsts = "This user does not exists!";
        public const string OtpOrEmailIsNotCorrect = "OTP or Email is invalid!";
        public const string SomethingWrong = "User or password is incorrect. please check your input and try again!";
        public const string OtpInvalid = "This opt code is invalid!";
        public const string OtpInvalidOrExpire = "This opt code is invalid or expired!";
        public const string AccountInactive = "This account is inactive!";
        public const string VoucherCodeInvalid = "This voucher code is invalid!";
        public const string OtpTryAgain = "Please try again after 1 minutes!";
        public const string TooMuchIncorectTry = "You have failed to supply the correct information too many times. Please contact your system administrator!";
    }

    public static class APIResponseMessage
    {
        public const string TemporaryPasswordNotCorrect = "Your temporary password not correct or expired";
        public const string EmailNotExist = "Your email not exist in our application";
        public const string UploadPhotoFailed = "Failed to update avatar";
        public const string ChooseFile = "Please chose file to upload";
        public const string MaximumSize = "The maximum size of file upload is 3Mb";
        public const string FileTypeInvalid = "The file type is invalid";
    }

}
