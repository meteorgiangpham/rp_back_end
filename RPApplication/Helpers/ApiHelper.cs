﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using RPApplication.ApiModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RPApplication.Helpers
{
    public static class ApiHelper
    {
        public static string Encrypt(string text)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    var appSettingsSection = Startup.StaticConfig.GetSection(Constants.AppSettings);
                    var secret = appSettingsSection[Constants.Secret].ToString();
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(secret));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateEncryptor())
                    {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(text);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }

        public static string Decrypt(string cipher)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    var appSettingsSection = Startup.StaticConfig.GetSection(Constants.AppSettings);
                    var secret = appSettingsSection[Constants.Secret].ToString();
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(secret));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateDecryptor())
                    {
                        byte[] cipherBytes = Convert.FromBase64String(cipher);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }

        public static void SendRegisterEmail(string emailTo, string firstName, string otpCode)
        {
            string body = string.Empty;
            var path = Path.Combine(Directory.GetCurrentDirectory(), Constants.EmailTemplateFolder, Constants.TemplateRegisterEmail);
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{FirstName}", firstName);
            body = body.Replace("{OtpCode}", otpCode);

            SendEmail(emailTo, Constants.SubjectAccountVerifyEmail, body);
        }

        public static void SendWelcomeEmail(string emailTo, string firstName)
        {
            string body = string.Empty;
            var path = Path.Combine(Directory.GetCurrentDirectory(), Constants.EmailTemplateFolder, Constants.TemplateWelcomeEmail);
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{FirstName}", firstName);

            SendEmail(emailTo, Constants.SubjectWelcomeEmail, body);
        }

        public static void SendForgotPasswordEmail(string emailTo, string firstName, string temporaryPassword)
        {
            string body = string.Empty;
            var path = Path.Combine(Directory.GetCurrentDirectory(), Constants.EmailTemplateFolder, Constants.TemplateForgotPasswordEmail);
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{FirstName}", firstName);
            body = body.Replace("{TemporaryPassword}", temporaryPassword);

            SendEmail(emailTo, Constants.SubjectResetPasswordRequestEmail, body);
        }

        public static void SendConfirmResetPasswordEmail(string emailTo, string firstName)
        {
            string body = string.Empty;
            var path = Path.Combine(Directory.GetCurrentDirectory(), Constants.EmailTemplateFolder, Constants.TemplateConfirmChangePassword);
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{FirstName}", firstName);

            SendEmail(emailTo, Constants.SubjectResetPasswordConfirmEmail, body);
        }

        public static void SendConfirmChangePasswordEmail(string emailTo, string firstName)
        {
            string body = string.Empty;
            var path = Path.Combine(Directory.GetCurrentDirectory(), Constants.EmailTemplateFolder, Constants.TemplateForgotPasswordEmail);
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{FirstName}", firstName);

            SendEmail(emailTo, Constants.SubjectChangePasswordConfirmEmail, body);
        }

        public static void ResendOTPEmail(string emailTo, string firstName, string otp)
        {
            string body = string.Empty;
            var path = Path.Combine(Directory.GetCurrentDirectory(), Constants.EmailTemplateFolder, Constants.TemplateResendOTP);
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
           
            body = body.Replace("{FirstName}", firstName);
            body = body.Replace("{OtpCode}", otp);

            SendEmail(emailTo, Constants.SubjectAccountVerifyEmail, body);
        }

        public static void SendLoginOTPEmail(string emailTo, string firstName, string otp)
        {
            string body = string.Empty;
            var path = Path.Combine(Directory.GetCurrentDirectory(), Constants.EmailTemplateFolder, Constants.TemplateLoginOTP);
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{FirstName}", firstName);
            body = body.Replace("{OtpCode}", otp);

            SendEmail(emailTo, Constants.SubjectAccountVerifyEmail, body);
        }

        public static void SendConfirmRedeemEmail(string emailTo, string firstName, string voucherImage, string voucherExpired)
        {
            string body = string.Empty;
            var path = Path.Combine(Directory.GetCurrentDirectory(), Constants.EmailTemplateFolder, Constants.TemplateConfirmRedeem);
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{FirstName}", firstName);
            body = body.Replace("{VoucherImage}", voucherImage);
            body = body.Replace("{VoucherExpired}", voucherExpired);

            SendEmail(emailTo, Constants.SubjectConfirmRedeemVoucher, body);
        }


        public static void SendEmail(string emailTo, string subject, string body)
        {
            try
            {
                var appSettingsSection = Startup.StaticConfig.GetSection(Constants.AppSettings);
                var smtpClient = appSettingsSection["SmtpClient"].ToString();
                var smtpPort = appSettingsSection["SmtpPort"].ToString();
                var smtpUserName = appSettingsSection["SmtpUserName"].ToString();
                var smtpPassword = appSettingsSection["SmtpPassword"].ToString();
                var msg = new MailMessage(appSettingsSection["EmailFrom"].ToString(), emailTo, subject, body);
                msg.IsBodyHtml = true;
                //var client = new SmtpClient(smtpClient, int.Parse(smtpPort));
               
                var client = new SmtpClient(appSettingsSection["SmtpClient"].ToString(), 587)
                {
                    Credentials = new NetworkCredential(appSettingsSection["SmtpUserName"].ToString(), appSettingsSection["SmtpPassword"].ToString()),
                    EnableSsl = true
                };

                client.Send(msg);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string GenerateRandomOTP(int iOTPLength)

        {
            string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            string sOTP = String.Empty;

            string sTempChars = String.Empty;

            Random rand = new Random();

            for (int i = 0; i < iOTPLength; i++)

            {

                int p = rand.Next(0, saAllowedCharacters.Length);

                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];

                sOTP += sTempChars;

            }

            return sOTP;

        }

        public static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null) opts = new PasswordOptions()
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
            "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
            "abcdefghijkmnopqrstuvwxyz",    // lowercase
            "0123456789",                   // digits
            "!@$?_-"                        // non-alphanumeric
        };

            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }

        public static bool IsValidEmail(this string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    string domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static string UploadFile(IFormFile files, Guid id)
        {
            
            var fileExtension = System.IO.Path.GetExtension(files.FileName);
            var lstValidExtension = new List<string> { ".jpg", ".mp4", ".png", ".JPG", ".PNG" };
            if (lstValidExtension.Contains(fileExtension))
            {
                try
                {
                    if (fileExtension == ".mp4")
                    {
                        if (files.Length > 2147483648)
                            throw new Exception("The file " + files.FileName + " over 2GB");
                    }
                    else
                    {
                        if (files.Length > 5 * 1024 * 1024)
                            throw new Exception("The file " + files.FileName + " over 5MB");
                    }
                    string rootPath = Path.Combine(Directory.GetCurrentDirectory(), Constants.UploadFolder, id.ToString());
                    if (!Directory.Exists(rootPath))
                    {
                        Directory.CreateDirectory(rootPath);
                    }
                    var timeStamp = DateTime.Now.ToFileTime();
                    var fileName = timeStamp + files.FileName;
                    using (FileStream filestream = System.IO.File.Create(Path.Combine(rootPath, fileName)))
                    {
                        files.CopyTo(filestream);
                        filestream.Flush();
                    }
                    return fileName;
                }catch (Exception ex)
                {
                    throw new Exception("Failed to upload file "+ files.FileName);
                }
            }
            else
            {
                throw new Exception("The file extension is invalid");
            }
        }

        public static async Task DeleteFileAsync(Guid id, string fileName)
        {

            try
            {
                string rootPath = Path.Combine(Directory.GetCurrentDirectory(), Constants.UploadFolder, id.ToString());
                string filePath = Path.Combine(rootPath, fileName);
                if (File.Exists(filePath))
                {
                    FileInfo ff = new FileInfo(filePath);
                    if (!ApiHelper.IsFileOpen(ff))
                    {
                        await Task.Run(() => {
                            File.Delete(filePath);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to delete file " + fileName + ".Details:"+ ex.Message);
            }
        }

        public static List<string> ReturnLstMedia(string file, string domain, Guid id)
        {
            if (!string.IsNullOrEmpty(file))
            {
                var lst = new List<string>();
                var lstFile = file.Split(';');
                foreach (var item in lstFile)
                {
                    lst.Add(domain + id + "/" + item);
                }
                return lst;
            }
            return new List<string>();
        }

        public static List<MediaThumbnailData> ReturnFullPathFile(string mediaFile, string domain)
        {
            var convertToJsonObject = JsonSerializer.Deserialize<List<MediaThumbnailData>>(mediaFile);
            foreach(var item in convertToJsonObject)
            {
                item.MediaUrl = !string.IsNullOrEmpty(item.MediaUrl) ? domain + item.MediaUrl : item.MediaUrl;
            }
            return convertToJsonObject != null ? convertToJsonObject.Where(x => x.MediaUrl != null).ToList() : convertToJsonObject;
        }

        public static bool IsFileOpen(this FileInfo f)
        {
            FileStream stream = null;

            try
            {
                stream = f.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null) stream.Close();
            }

            return false;
        }

    }
}
