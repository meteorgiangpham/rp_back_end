﻿using AutoMapper;
using RPApplication.ApiModels;
using RPApplication.Models;

namespace RPApplication.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserProfileResponse>();
          
        }
    }
}
