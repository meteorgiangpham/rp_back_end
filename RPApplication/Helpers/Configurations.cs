﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RPApplication.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }

        public string ConnectionString { get; set; }

        public double TokenExpireTime { get; set; }

        public string SecretAdminPassword { get; set; }

    }
}
