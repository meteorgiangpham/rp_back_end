﻿namespace RPApplication.Helpers
{
    public enum VerifyOtpStage
    {
        Login = 1,
        Register = 2,
        ChangePassword = 3
    }

    public enum ModuleType
    {
        FacadeA = 1,
        CorridorB = 2,
    }
}
