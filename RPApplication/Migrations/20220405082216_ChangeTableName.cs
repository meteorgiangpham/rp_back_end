﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RPApplication.Migrations
{
    public partial class ChangeTableName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuditTrials_Users_UserId",
                table: "AuditTrials");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AuditTrials",
                table: "AuditTrials");

            migrationBuilder.RenameTable(
                name: "AuditTrials",
                newName: "AuditTrails");

            migrationBuilder.RenameIndex(
                name: "IX_AuditTrials_UserId",
                table: "AuditTrails",
                newName: "IX_AuditTrails_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AuditTrails",
                table: "AuditTrails",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AuditTrails_Users_UserId",
                table: "AuditTrails",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuditTrails_Users_UserId",
                table: "AuditTrails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AuditTrails",
                table: "AuditTrails");

            migrationBuilder.RenameTable(
                name: "AuditTrails",
                newName: "AuditTrials");

            migrationBuilder.RenameIndex(
                name: "IX_AuditTrails_UserId",
                table: "AuditTrials",
                newName: "IX_AuditTrials_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AuditTrials",
                table: "AuditTrials",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AuditTrials_Users_UserId",
                table: "AuditTrials",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
