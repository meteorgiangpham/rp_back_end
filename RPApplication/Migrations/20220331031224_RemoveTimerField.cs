﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RPApplication.Migrations
{
    public partial class RemoveTimerField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Timer",
                table: "ScreenSavers");

            migrationBuilder.DropColumn(
                name: "Timer",
                table: "AuditTrials");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Timer",
                table: "ScreenSavers",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "Timer",
                table: "AuditTrials",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
