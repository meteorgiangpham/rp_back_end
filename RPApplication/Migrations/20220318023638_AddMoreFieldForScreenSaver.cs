﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RPApplication.Migrations
{
    public partial class AddMoreFieldForScreenSaver : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ThumbnailImage",
                table: "ScreenSavers",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ThumbnailImage",
                table: "ScreenSavers");
        }
    }
}
