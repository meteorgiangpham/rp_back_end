﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RPApplication.ApiModels;
using RPApplication.Services;
using System;
using System.Linq;
using System.Security.Claims;

namespace RPApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class PlayersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private IStationService _stationService;
        private readonly AutoMapper.IMapper _mapper;

        public PlayersController(ILogger<UsersController> logger, IStationService stationService, AutoMapper.IMapper mapper)
        {
            _logger = logger;
            _stationService = stationService;
            _mapper = mapper;
        }


        [HttpGet("get-station-1")]
        public IActionResult GetStationFacade()
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var result = _stationService.GetStationFacade(userId, domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false
                }); ;
            }
        }

        [HttpGet("get-station-2")]
        public IActionResult GetStationCorridor()
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var result = _stationService.GetStationCorridor(userId, domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false
                }); ;
            }
        }
    }
}
