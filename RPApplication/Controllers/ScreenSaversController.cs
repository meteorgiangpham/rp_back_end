﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Services;
using System;
using System.Linq;
using System.Security.Claims;

namespace RPApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class ScreenSaversController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private IScreenSaverService _screenSaverService;
        private readonly AutoMapper.IMapper _mapper;

        public ScreenSaversController(ILogger<UsersController> logger, IScreenSaverService screenSaverService, AutoMapper.IMapper mapper)
        {
            _logger = logger;
            _screenSaverService = screenSaverService;
            _mapper = mapper;
        }

        //[HttpPost("create-screen-saver-facade-a")]
        //public IActionResult CreateScreenSaverFacadeA([FromForm] CreateScreenSaverFacadeARequest model)
        //{
        //    try
        //    {
        //        var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
        //        var id = Guid.NewGuid();
        //        if (model.LedVideo != null && model.LedVideo.Length > 0)
        //        {
        //            ApiHelper.UploadFile(model.LedVideo, id);
        //            model.LedVideoUrl = model.LedVideo.FileName;
        //        }
        //        if (model.ThumbnailImage != null && model.ThumbnailImage.Length > 0)
        //        {
        //            ApiHelper.UploadFile(model.ThumbnailImage, id);
        //            model.ThumbnailUrl = model.ThumbnailImage.FileName;
        //        }
        //        if (model.Media != null && model.Media.Length > 0)
        //        {
        //            ApiHelper.UploadFile(model.Media, id);
        //            model.MediaUrl = model.Media.FileName;
        //        }
        //        var result = _screenSaverService.CreateScreenSaverFacadeA(model, id, userId);
        //        return Ok(new ApiResponse
        //        {
        //            Success = true,
        //            Data = result
        //        }); ;
        //    }
        //    catch (Exception e)
        //    {
        //        return Ok(new ApiResponse
        //        {
        //            Success = false,
        //            Data = e.Message
        //        }); ;
        //    }
        //}

        [HttpPut("update-screen-saver-facade-a")]
        public IActionResult UpdateScreenSaverFacadeA([FromForm] UpdateScreenSaverFacadeARequest model)
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                if (model.LedVideo != null && model.LedVideo.Length > 0)
                {
                    var ledVideoName = ApiHelper.UploadFile(model.LedVideo, model.Id);
                    model.LedVideoUrl = ledVideoName;
                }
                if (model.ThumbnailImage != null && model.ThumbnailImage.Length > 0)
                {
                    var thumbnailName = ApiHelper.UploadFile(model.ThumbnailImage, model.Id);
                    model.ThumbnailUrl = thumbnailName;
                }
                if (model.Media != null && model.Media.Length > 0)
                {
                    var mediaName = ApiHelper.UploadFile(model.Media, model.Id);
                    model.MediaUrl = mediaName;
                }
                var result = _screenSaverService.UpdateScreenSaverFacadeA(model, userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result,
                    Message = "Update screen saver successfully!"
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        //[HttpPost("create-screen-saver-corridor-b")]
        //public IActionResult CreateScreenSaverCorridorB([FromForm] CreateScreenSaverCorrdiorBRequest model)
        //{
        //    try
        //    {
        //        var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
        //        var id = Guid.NewGuid();
        //        if (model.Media != null && model.Media.Length > 0)
        //        {
        //            ApiHelper.UploadFile(model.Media, id);
        //            model.MediaUrl = model.Media.FileName;
        //        }
        //        var result = _screenSaverService.CreateScreenSaverCorridorB(model, id, userId);
        //        return Ok(new ApiResponse
        //        {
        //            Success = true,
        //            Data = result
        //        }); ;
        //    }
        //    catch (Exception e)
        //    {
        //        return Ok(new ApiResponse
        //        {
        //            Success = false,
        //            Data = e.Message
        //        }); ;
        //    }
        //}

        [HttpPut("update-screen-saver-corridor-b")]
        public IActionResult UpdateScreenSaverCorridorB([FromForm] UpdateScreenSaverCorridiorBRequest model)
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                if (model.Media != null && model.Media.Length > 0)
                {
                    var mediaName = ApiHelper.UploadFile(model.Media, model.Id);
                    model.MediaUrl = mediaName;
                }
                var result = _screenSaverService.UpdateScreenSaverCorridorB(model, userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result,
                    Message = "Update screen saver successfully!"
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = e.Message
                }); ;
            }
        }

        [HttpGet("get-screen-saver")]
        public IActionResult GetStationPage(Guid stationId)
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var result = _screenSaverService.GetAllScreenSaver(stationId, domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = e.Message
                }); ;
            }
        }

        [HttpGet("get-screen-saver-by-id")]
        public IActionResult GetScreenSaverById(Guid stationId, Guid id)
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var result = _screenSaverService.GetScreenSaverById(stationId, id, domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = e.Message
                }); ;
            }
        }

        [HttpPut("update-led-screen-saver")]
        public IActionResult UpdateLedScreenSaver([FromForm] UpdateLedScreenSaverRequest model)
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                if (model.LedVideo != null && model.LedVideo.Length > 0)
                {
                    var ledName = ApiHelper.UploadFile(model.LedVideo, model.Id);
                    model.LedVideoUrl = ledName;
                }
                if (model.Thumbnail != null && model.Thumbnail.Length > 0)
                {
                    var thumbnailName = ApiHelper.UploadFile(model.Thumbnail, model.Id);
                    model.ThumbnailUrl = thumbnailName;
                }
                var result = _screenSaverService.UpdateLedScreenSaver(model, userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result,
                    Message = "Update led screen saver successfully!"
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpGet("get-led-screen-saver-by-id")]
        public IActionResult GetLedScreenSaverById(Guid stationId, Guid id)
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var result = _screenSaverService.GetLedScreenSaverById(stationId, id, domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = e.Message
                }); ;
            }
        }

        [HttpGet("get-led-screen-saver")]
        public IActionResult GetLedScreenSaver(Guid stationId)
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var result = _screenSaverService.GetAllLedScreenSaver(stationId, domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = e.Message
                }); ;
            }
        }
    }
}
