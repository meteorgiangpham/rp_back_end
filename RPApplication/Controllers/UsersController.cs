﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Models;
using RPApplication.Services;
using System;
using System.Linq;
using System.Security.Claims;

namespace RPApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private IUserService _userService;
        private readonly AutoMapper.IMapper _mapper;

        public UsersController(ILogger<UsersController> logger, IUserService userService, AutoMapper.IMapper mapper)
        {
            _logger = logger;
            _userService = userService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login(AuthenticateRequest model)
        {
            try
            {
                var username = model.UserName;
                var password = model.Password;
                var result = _userService.Login(username, ApiHelper.Encrypt(password));
                HttpContext.Session.SetString("RPUser", result.Token);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpPost("create-admin-account")]
        [AllowAnonymous]
        public IActionResult CreateAdminAccount()
        {
            try
            {
                var result = _userService.CreateAdminAccount();
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpPost("register")]
        public IActionResult CreateNewUser(RegisterUserRequest request)
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                var result = _userService.RegisterUser(request, userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpPost("change-password")]
        public IActionResult ChangePassword(ChangePasswordRequest request)
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                _userService.ChangePassword(request, userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                });
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpPost("admin-change-password")]
        public IActionResult AdminChangePassword(AdminChangePasswordRequest request)
        {
            try
            {
                _userService.AdminChangePassword(request);
                return Ok(new ApiResponse
                {
                    Success = true,
                });
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }


        [HttpPost("logout")]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var audit = new AuditTrail()
            {
                UserId = userId,
                Action = "User logout",
                Detail = "User logout from system",
            };
            _userService.SaveAuditTrail(audit);
            return Ok(new ApiResponse
            {
                Success = true
            });
        }

        [HttpGet("audit-trail")]
        public IActionResult GetALlAuditTrial()
        {
            return Ok(new ApiResponse
            {
                Success = true,
                Data = _userService.GetAuditTrails()
            });
        }

        [HttpGet("list-user")]
        public IActionResult GetAllUser()
        {
            var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            return Ok(new ApiResponse
            {
                Success = true,
                Data = _userService.GetListUser(userId)
            });
        }


        [HttpGet("get-user-by-id")]
        public IActionResult GetUserById(Guid userId)
        {
            return Ok(new ApiResponse
            {
                Success = true,
                Data = _userService.GetUserById(userId)
            });
        }

        [HttpPut("update-user")]
        public IActionResult UpdateUser(UpdateUser updateUser)
        {
            try
            {
                var currentUserId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = _userService.UpdateUser(updateUser, currentUserId, fullName),
                    Message = "Update user successfully!"
                });
            }catch (Exception ex)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = ex.Message,
                });
            }
        }

        [HttpDelete("delete-user")]
        public IActionResult DeleteUser(Guid userId)
        {
            try
            {

                var currentUserId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                _userService.DeleteUser(userId, currentUserId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Message = "Delete user successfully!"
                });
            }catch (Exception ex)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = ex.Message
                });
            }
        }
    }
}
