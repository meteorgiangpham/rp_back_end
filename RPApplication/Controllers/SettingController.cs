﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;

namespace RPApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class SettingController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private ISettingService _settingService;
        public SettingController(ILogger<UsersController> logger, ISettingService settingService)
        {
            _logger = logger;
            _settingService = settingService;
        }

        [HttpPut("update-setting")]
        public IActionResult UpdatePage([FromForm] Setting request)
        {
            try
            {
                request.UpdatedByUserId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                _settingService.UpdateSetting(request);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Message = "Update setting successfully!"
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpGet("get-setting")]
        public IActionResult GetSetting()
        {
            try
            {

                var result = _settingService.GetSetting();
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false
                }); ;
            }
        }
    }
}
