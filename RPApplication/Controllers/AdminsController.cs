﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RPApplication.ApiModels;
using RPApplication.Helpers;
using RPApplication.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;

namespace RPApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class AdminsController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private IPageService _pageService;
        private IHotspotService _hotspotService;
        private IStationService _stationService;
        private readonly AutoMapper.IMapper _mapper;

        public AdminsController(ILogger<UsersController> logger, IPageService pageService, IHotspotService hotspotService, IStationService stationService, AutoMapper.IMapper mapper)
        {
            _logger = logger;
            _pageService = pageService;
            _hotspotService = hotspotService;
            _stationService = stationService;
            _mapper = mapper;
        }

        [HttpPut("update-station-page")]
        public IActionResult UpdatePage([FromForm] UpdatePageRequest model)
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                if (model.Background != null && model.Background.Length > 0)
                {
                    var backgroundName = ApiHelper.UploadFile(model.Background, model.Id);
                    model.BackgroundUrl = backgroundName;
                }

                if (model.ContentBlocks != null && model.ContentBlocks.Count() > 0)
                {
                    foreach (var block in model.ContentBlocks)
                    {
                        var blockId = block.Id == new Guid() ? Guid.NewGuid() : block.Id;
                        block.Id = blockId;
                        if (block != null && block.MediaThumbnails != null && block.MediaThumbnails.Count() > 0)
                        {
                            var lstMediaThumbnail = new List<MediaThumbnailData>();
                            foreach (var item in block.MediaThumbnails)
                            {
                                var media = item.Media != null && item.Media.Length > 0 ? ApiHelper.UploadFile(item.Media, blockId) : item.MediaUrl;
                                lstMediaThumbnail.Add(new MediaThumbnailData()
                                {
                                    MediaUrl = media,
                                });
                            }
                            block.MediaFile = JsonSerializer.Serialize(lstMediaThumbnail);
                        }
                    }
                }

                //if (model.ContentBlocks != null && model.ContentBlocks.Count() > 0)
                //{
                //    foreach (var block in model.ContentBlocks)
                //    {
                //        var blockId = block.Id == new Guid() ? Guid.NewGuid() : block.Id;
                //        block.Id = blockId;
                //        if (block.Media != null && block.Media.Count > 0)
                //        {
                //            var lstMediaFile = new List<string>();
                //            foreach(var media in block.Media)
                //            {
                //                var mediaName = ApiHelper.UploadFile(media, blockId);
                //                lstMediaFile.Add(mediaName);
                //            }
                //            block.MediaFile = string.Join(";", lstMediaFile);
                //        }
                //        if (block.Thumbnail != null && block.Thumbnail.Length > 0)
                //        {
                //            var thumbnailName = ApiHelper.UploadFile(block.Thumbnail, blockId);
                //            block.ThumbnailUrl = thumbnailName;
                //        }
                //    }
                //}

                var result = _pageService.UpdatePage(model, userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result,
                    Message = "Update page successfully!"
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpPut("update-background-hostpot")]
        public IActionResult UpdateBackgroundHostpot([FromForm] CreateHostpotBackgroundRequest model)
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                if (model.BackgroundHostpot != null && model.BackgroundHostpot.Length > 0)
                {
                    var backgroundName = ApiHelper.UploadFile(model.BackgroundHostpot, model.StationId);
                    model.BackgroundHostpotUrl = backgroundName;
                }

                var result = _hotspotService.CreateHostpotBackgroundImage(model, userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result,
                    Message = "Update background hostpot successfully!"
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpGet("get-background-host-pot")]
        public IActionResult GetBackgroundHostpot(Guid stationId)
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var result = _hotspotService.GetBackgroundHostpot(stationId, domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false
                }); ;
            }
        }

        [HttpGet("get-station-page")]
        public IActionResult GetAllPage()
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var result = _pageService.GetPageAll(domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false
                }); ;
            }
        }

        [HttpGet("get-station-page-by-id")]
        public IActionResult GetStationPage(Guid id)
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var result = _pageService.GetPageById(id, domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false
                }); ;
            }
        }

        [HttpPut("update-station-hostpot")]
        public IActionResult UpdateHostpot([FromForm] UpdateHostpotRequest model)
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                if (model.Media != null && model.Media.Length > 0)
                {
                    var mediaName = ApiHelper.UploadFile(model.Media, model.Id);
                    model.MediaUrl = mediaName;
                }
                if (model.HighLightMedia != null && model.HighLightMedia.Length > 0)
                {
                    var hightLightName = ApiHelper.UploadFile(model.HighLightMedia, model.Id);
                    model.HighlightMediaUrl = hightLightName;
                }
                if (model.Thumbnail != null && model.Thumbnail.Length > 0)
                {
                    var thumbnailName = ApiHelper.UploadFile(model.Thumbnail, model.Id);
                    model.ThumbnailUrl = thumbnailName;
                }

                var result = _hotspotService.UpdateHostpot(model, userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result,
                    Message = "Update hostpot successfully"
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }


        [HttpGet("get-station-host-pot")]
        public IActionResult GetAllHostpot()
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var result = _hotspotService.GetAllHostpot(domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = e.Message
                }); ;
            }
        }

        [HttpGet("get-station-hostpot-by-id")]
        public IActionResult GetHostpotById(Guid id)
        {
            try
            {
                string domain = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/Upload/";
                var result = _hotspotService.GetHostpotById(id, domain);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false
                }); ;
            }
        }
    }
}
