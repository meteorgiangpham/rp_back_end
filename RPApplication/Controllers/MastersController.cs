﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RPApplication.ApiModels;
using RPApplication.Services;
using System;
using System.Linq;
using System.Security.Claims;

namespace RPApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class MastersController : ControllerBase
    {
        private IPageService _pageService;
        private IHotspotService _hotspotService;
        private IStationService _stationService;
        private IScreenSaverService _screenSaverService;
        private IUserService _userService;

        public MastersController(IScreenSaverService screenSaverService, IPageService pageService, 
            IHotspotService hotspotService, IStationService stationService, IUserService userService)
        {
            _userService = userService;
            _pageService = pageService;
            _hotspotService = hotspotService;
            _stationService = stationService;
            _screenSaverService = screenSaverService;
        }

        [AllowAnonymous]
        [HttpPost("generate-admin-account")]
        public IActionResult GenerateAdminAccount()
        {
            try
            {
                var result = _userService.CreateAdminAccount();
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpGet("get-or-create-station")]
        public IActionResult GetAllStation()
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                var result = _stationService.GetOrCreateStationList(userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false
                }); ;
            }
        }

        [HttpPost("create-master-data-led-screen-saver")]
        public IActionResult CreateLedScreenSaverData()
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                var result = _screenSaverService.CreateMasterDataLedScreenSaver(userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpPost("create-master-data-screen-saver")]
        public IActionResult CreateScreenSaverData()
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                var result = _screenSaverService.CreateMasterDataScreenSaver(userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpPost("create-master-data-page")]
        public IActionResult CreatePage()
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                var result = _pageService.CreatePage(userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }

        [HttpPost("create-master-data-hostpot")]
        public IActionResult CreateHostpot()
        {
            try
            {
                var userId = Guid.Parse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                var fullName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                var result = _hotspotService.CreateHostpot(userId, fullName);
                return Ok(new ApiResponse
                {
                    Success = true,
                    Data = result
                }); ;
            }
            catch (Exception e)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Data = e.Message
                }); ;
            }
        }
    }
}
