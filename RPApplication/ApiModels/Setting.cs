﻿using System;

namespace RPApplication.ApiModels
{
    public class Setting
    {

        public bool ShowFacadeA { get; set; }

        public bool ShowCorridorB { get; set; }

        public Guid? UpdatedByUserId { get; set; }
    }
}
