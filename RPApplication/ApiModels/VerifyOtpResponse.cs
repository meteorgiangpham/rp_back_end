﻿namespace RPApplication.ApiModels
{
    public class VerifyOtpResponse
    {

        public string Token { get; set; }
    }
}
