﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class CreateScreenSaverCorrdiorBRequest
    {
        [Required]
        public Guid StationId { get; set; }
        [Required]
        [MaxLength(70)]
        public string Title { get; set; }
        [Required]
        [MaxLength(300)]
        public string Text { get; set; }
        [Required]
        public IFormFile Media { get; set; }
        public string MediaUrl { get; set; }
    }
}
