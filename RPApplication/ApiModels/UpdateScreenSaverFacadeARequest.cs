﻿using Microsoft.AspNetCore.Http;
using RPApplication.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class UpdateScreenSaverFacadeARequest
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Body { get; set; }
        public IFormFile LedVideo { get; set; }
        public IFormFile ThumbnailImage { get; set; }
        public IFormFile Media { get; set; }
        public string LedVideoUrl { get; set; }
        public string MediaUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public List<ImageDelete> ImageDeletes { get; set; }
    }
}
