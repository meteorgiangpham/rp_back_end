﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class UpdateHostpotRequest
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        public IFormFile HighLightMedia { get; set; }
        public string HighlightMediaUrl { get; set; }
        public IFormFile Media { get; set; }
        public string MediaUrl { get; set; }
        public IFormFile Thumbnail { get; set; }
        public string ThumbnailUrl { get; set; }
        [Required]
        public string CoordinateX { get; set; }
        [Required]
        public string CoordinateY { get; set; }
        [Required]
        public bool IsVisible { get; set; }
        public List<ImageDelete> ImageDeletes { get; set; }
    }

    public class ImageDelete
    {
        public Guid Id { get; set;}
        public string FileName { get; set; }
    }
}
