﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class ChangePasswordRequest
    {

        [Required]
        public string OldPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string ConfirmNewPassword { get; set; }
    }

    public class AdminChangePasswordRequest
    {
        [Required]
        public string SecretAdminPassword { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string ConfirmNewPassword { get; set; }
    }
}
