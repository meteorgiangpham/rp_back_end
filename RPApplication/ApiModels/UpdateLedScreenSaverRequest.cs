﻿using Microsoft.AspNetCore.Http;
using RPApplication.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class UpdateLedScreenSaverRequest
    {
        [Required]
        public Guid Id { get; set; }
        public IFormFile LedVideo { get; set; }
        public string LedVideoUrl { get; set; }
        public IFormFile Thumbnail { get; set; }
        public string ThumbnailUrl { get; set; }
        public List<ImageDelete> ImageDeletes { get; set; }
    }
}
