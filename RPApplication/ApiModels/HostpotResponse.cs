﻿using System;

namespace RPApplication.ApiModels
{
    public class HostpotResponse
    {
        public Guid Id { get; set; }
        public Guid StationId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string HighlightMediaUrl { get; set; }
        public string HostpotImageUrl { get; set; }
        public string BackgroundHostpotImage { get; set; }
        public string ThumbnailUrl { get; set; }
        public string MediaUrl { get; set; }
        public string CoordinateX { get; set; }
        public string CoordinateY { get; set; }
        public bool IsVisible { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
