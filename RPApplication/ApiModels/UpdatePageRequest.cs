﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class UpdatePageRequest
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public IFormFile Background { get; set; }
        public string BackgroundUrl { get; set; }
        [Required]
        public bool IsVisible { get; set; }
        public List<ContentBlockRequest> ContentBlocks { get; set; }
        public List<ImageDelete> ImageDeletes { get; set; }
        public List<ContentBlockDelete> ContentBlockDeletes { get; set; }
    }

    public class ContentBlockDelete
    {
        public Guid PageId { get; set; }
        public Guid ContentBlockId { get; set; }
    }
}
