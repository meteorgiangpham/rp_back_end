﻿using System;

namespace RPApplication.ApiModels
{
    public class HostpotBackgroundResponse
    {
        public Guid StationId { get; set; }
        public string BackgroundHostpotUrl { get; set; }
    }
}
