﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class CreateHostpotRequest
    {
        [Required]
        public Guid StationId { get; set; }
        [Required]
        [MaxLength(80)]
        public string Title { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Body { get; set; }
        [Required]
        public IFormFile HighLightMedia { get; set; }
        public string HighlightMediaUrl { get; set; }
        [Required]
        public IFormFile Media { get; set; }
        public string MediaUrl { get; set; }
        [Required]
        public string CoordinateX { get; set; }
        [Required]
        public string CoordinateY { get; set; }
        [Required]
        public bool IsVisible { get; set; }
    }
}
