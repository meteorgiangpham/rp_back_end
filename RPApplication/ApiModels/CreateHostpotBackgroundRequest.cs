﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class CreateHostpotBackgroundRequest
    {
        [Required]
        public Guid StationId { get; set; }
        public IFormFile BackgroundHostpot { get; set; }
        public string BackgroundHostpotUrl { get; set; }
    }
}
