﻿using System;

namespace RPApplication.ApiModels
{
    public class LedScreenSaverResponse
    {
        public Guid Id { get; set; }
        public string LedVideoUrl { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
