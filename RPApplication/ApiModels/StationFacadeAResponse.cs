﻿using RPApplication.Helpers;
using System;
using System.Collections.Generic;

namespace RPApplication.ApiModels
{
    public class StationFacadeAResponse
    {
        public Guid Id { get; set; }
        public ModuleType ModuleType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LedVideoUrl { get; set; }
        public string BackgrounHostpotUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public ScreenSaverResponse ScreenSaver { get; set; }
        public IEnumerable<HostpotResponse> Hostpots { get; set; }
    }
}
