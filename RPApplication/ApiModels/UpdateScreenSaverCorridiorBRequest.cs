﻿using Microsoft.AspNetCore.Http;
using RPApplication.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class UpdateScreenSaverCorridiorBRequest
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        [MaxLength(80)]
        public string Title { get; set; }
        [MaxLength(300)]
        public string Text { get; set; }
        public IFormFile Media { get; set; }
        public string MediaUrl { get; set; }
        public List<ImageDelete> ImageDeletes { get; set; }
    }
}
