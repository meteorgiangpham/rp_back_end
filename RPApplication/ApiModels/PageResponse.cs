﻿using System;
using System.Collections.Generic;

namespace RPApplication.ApiModels
{
    public class PageResponse
    {
        public Guid Id { get; set; }
        public Guid StationId { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public string BackgroundImageUrl { get; set; }
        public bool IsVisible { get; set; }
        public IEnumerable<ContentBlockResponse> ContentBlocks { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class ContentBlockResponse
    {
        public Guid Id { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }
        public bool IsShowMedia { get; set; }
        public List<MediaThumbnailData> ListMediaFile { get; set; }
    }
}
