﻿using System;

namespace RPApplication.ApiModels
{
    public class ScreenSaverResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string LedVideoUrl { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public string MediaUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
