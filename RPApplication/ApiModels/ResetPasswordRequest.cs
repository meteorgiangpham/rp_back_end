﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class ResetPasswordRequest
    {
        [Required]
        public string Email{ get; set; }

        [Required]
        public string TemporaryPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string ConfirmNewPassword { get; set; }
    }
}
