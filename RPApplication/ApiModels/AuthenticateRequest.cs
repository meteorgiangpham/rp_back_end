﻿using RPApplication.Helpers;
using System;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class AuthenticateRequest
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
