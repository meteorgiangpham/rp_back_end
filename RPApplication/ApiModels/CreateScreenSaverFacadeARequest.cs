﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class CreateScreenSaverFacadeARequest
    {
        [Required]
        public Guid StationId { get; set; }
        [Required]
        [MaxLength(70)]
        public string Title { get; set; }
        [MaxLength(300)]
        public string Body { get; set; }
        [Required]
        public IFormFile LedVideo { get; set; }
        [Required]
        public IFormFile ThumbnailImage { get; set; }
        [Required]
        public IFormFile Media { get; set; }
        public string LedVideoUrl { get; set; }
        public string MediaUrl { get; set; }
        public string ThumbnailUrl { get; set; }
    }
}
