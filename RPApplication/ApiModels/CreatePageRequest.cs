﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RPApplication.ApiModels
{
    public class CreatePageRequest
    {
        [Required]
        public Guid StationId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string SubTitle { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public IFormFile Background { get; set; }
        public string BackgroundUrl { get; set; }
        [Required]
        public bool IsVisible { get; set; }
        public IEnumerable<ContentBlockRequest> ContentBlocks { get; set; }
    }

    public class ContentBlockRequest
    {
        public Guid Id { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }
        public bool IsShowMedia { get; set; }
        public string MediaFile { get; set; }
        public List<MediaList> MediaThumbnails { get; set; }
    }

    public class MediaList
    {
        public IFormFile Media { get; set; } = null;
        public string MediaUrl { get; set; }
    }

    public class MediaThumbnailData
    {
        public string MediaUrl { get; set; }
    }
}
